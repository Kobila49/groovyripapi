import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthGuard} from '@app/auth/guards';
import {JwtInterceptor} from '@app/auth/helpers';
import {AuthenticationService} from '@app/auth/services';


import {StaticModule} from '@app/static';
import {ModalModule} from 'ngx-modialog';
import {BootstrapModalModule} from 'ngx-modialog/plugins/bootstrap';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from './core';

import {SharedModule} from './shared';
import {ToastrModule} from "ngx-toastr";


@NgModule({
  imports: [
    //angular
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    ToastrModule.forRoot(),
    //core & shared
    CoreModule,
    SharedModule,

    // features
    StaticModule,

    // app
    AppRoutingModule
  ],
  declarations: [AppComponent],
  providers: [
    AuthGuard,
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
