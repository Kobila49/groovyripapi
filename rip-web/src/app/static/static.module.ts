import {NgModule} from '@angular/core';
import {SharedModule} from '@app/shared';
import {HomeComponent} from '@app/static/home/home.component';
import {StaticRoutingModule} from '@app/static/static-routing.module';
import {WorkingHoursComponent} from './working-hours/working-hours.component';
import {MenuComponent} from './menu/menu.component';
import {AccountDetailsComponent} from './account-details/account-details.component';
import {AccountOrdersComponent} from './account-orders/account-orders.component';
import {ConfirmDialogComponent} from '@app/static/menu/confirm-dialog/confirm-dialog.component';
import {EditMealComponent} from './menu/edit-meal/edit-meal.component';
import {CreateMealComponent} from './menu/create-meal/create-meal.component';
import {MealFormComponent} from './menu/meal-form/meal-form.component';
import {UsersComponent} from './users/users.component';
import {CreateUserComponent} from './users/create-user/create-user.component';
import {EditUserComponent} from './users/edit-user/edit-user.component';
import { DeliveryCompaniesComponent } from './delivery-companies/delivery-companies.component';
import { DeliveryCompaniesAddComponent } from './delivery-companies/delivery-companies-add/delivery-companies-add.component';
import { DeliveryCompaniesEditComponent } from './delivery-companies/delivery-companies-edit/delivery-companies-edit.component';

@NgModule({
  imports: [
    SharedModule,
    StaticRoutingModule
  ],
  declarations: [HomeComponent,
    WorkingHoursComponent,
    MenuComponent,
    AccountDetailsComponent,
    AccountOrdersComponent,
    ConfirmDialogComponent,
    EditMealComponent,
    CreateMealComponent,
    MealFormComponent,
    UsersComponent,
    CreateUserComponent,
    EditUserComponent,
    DeliveryCompaniesComponent,
    DeliveryCompaniesAddComponent,
    DeliveryCompaniesEditComponent,
  ],
  entryComponents: [ConfirmDialogComponent]
})
export class StaticModule {
}
