import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryCompaniesComponent } from './delivery-companies.component';

describe('DeliveryCompaniesComponent', () => {
  let component: DeliveryCompaniesComponent;
  let fixture: ComponentFixture<DeliveryCompaniesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryCompaniesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryCompaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
