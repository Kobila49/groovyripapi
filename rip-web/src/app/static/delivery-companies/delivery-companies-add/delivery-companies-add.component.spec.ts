import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryCompaniesAddComponent } from './delivery-companies-add.component';

describe('DeliveryCompaniesAddComponent', () => {
  let component: DeliveryCompaniesAddComponent;
  let fixture: ComponentFixture<DeliveryCompaniesAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryCompaniesAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryCompaniesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
