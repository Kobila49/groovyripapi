import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {AppConstants} from "@app/common/app.constants";
import {delay} from "@app/shared/util/delay";
import {DeliveryCompany} from "@app/auth/models/delivery-company.model";
import {DeliveryCompanyService} from "@app/static/shared/delivery-company.service";

@Component({
  selector: 'rip-delivery-companies-add',
  templateUrl: './delivery-companies-add.component.html',
  styleUrls: ['./delivery-companies-add.component.scss']
})
export class DeliveryCompaniesAddComponent implements OnInit {

  create: FormGroup;
  company: DeliveryCompany;
  validation: object;
  errorMessages: string[];

  constructor(
    private fb: FormBuilder,
    private service: DeliveryCompanyService,
    private toastrService: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.errorMessages = [];
    this.validation = {
      numberPattern: AppConstants.validationErrorMessages.numberPattern,
      required: AppConstants.validationErrorMessages.required
    };
    this.createForm();
  }

  createForm() {
    this.create = this.fb.group({
      name: ['', Validators.required],
      price: ['', [Validators.required, Validators.pattern(AppConstants.validation.number.pattern)]],
      time: ['', [Validators.required, Validators.pattern(AppConstants.validation.number.pattern)]]
    });
  }

  onSubmit() {
    if (this.create.invalid) {
      for (let controlName in this.create.controls) {
        this.create.get(controlName)
          .markAsTouched();
      }
      return;
    }
    this.errorMessages = [];
    const deliveryCompany = this.service.prepareCompany(this.create.value);
    this.service.createCompany(deliveryCompany)
      .subscribe(
        (response) => {
          this.company = response;
          this.toastrService.success('Uspješno ste spremili dostavljača!');
          delay(2000).then(() => this.router.navigate(['/delivery-companies']));
        },
        (error) => {
          this.toastrService.error("Greška prilikom spremanja dostavljača, greška: " + error);
          console.log(this.errorMessages);
        });
  }

  get f() {
    return this.create.controls;
  };

}
