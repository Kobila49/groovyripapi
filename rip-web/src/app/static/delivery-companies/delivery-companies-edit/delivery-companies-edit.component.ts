import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute, Router} from "@angular/router";
import {AppConstants} from "@app/common/app.constants";
import {switchMap} from "rxjs/operators";
import {delay} from "@app/shared/util/delay";
import {DeliveryCompany} from "@app/auth/models/delivery-company.model";
import {DeliveryCompanyService} from "@app/static/shared/delivery-company.service";

@Component({
  selector: 'rip-delivery-companies-edit',
  templateUrl: './delivery-companies-edit.component.html',
  styleUrls: ['./delivery-companies-edit.component.scss']
})
export class DeliveryCompaniesEditComponent implements OnInit {

  editForm: FormGroup;
  disabled: boolean;
  company: DeliveryCompany;
  validation: object;
  errorMessages: string[];

  constructor(
    private fb: FormBuilder,
    private companyService: DeliveryCompanyService,
    private toastrService: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.errorMessages = [];
    this.validation = {
      numberPattern: AppConstants.validationErrorMessages.numberPattern,
      required: AppConstants.validationErrorMessages.required
    };
    this.route.paramMap.pipe(
      switchMap(params => {
          const id = params.get('id');
          return this.companyService.fetchCompanyById(id);
        }
      )
    ).subscribe((company: DeliveryCompany) => {
      this.company = company;
      this.createEditForm(company);
    });
  }

  createForm() {
    this.editForm = this.fb.group({
      name: ['', Validators.required],
      price: ['', [Validators.required, Validators.pattern(AppConstants.validation.number.pattern)]],
      time: ['', [Validators.required, Validators.pattern(AppConstants.validation.number.pattern)]]
    });
  }

  createEditForm(company: DeliveryCompany) {
    this.createForm();
    this.editForm.setValue({
      name: company.name,
      time: company.time,
      price: company.price
    });
  }

  onSubmit() {
    if (this.editForm.invalid) {
      for (let controlName in this.editForm.controls) {
        this.editForm.get(controlName)
          .markAsTouched();
      }
      return;
    }
    this.errorMessages = [];
    const preparedCompany = this.companyService.prepareCompany(this.editForm.value);
    this.companyService.editUser(preparedCompany, this.company.id)
      .subscribe(
        (response) => {
          this.company = response;
          this.toastrService.success('Uspješno ste uredili dostavljača!');
          delay(2000).then(() => this.router.navigate(['/delivery-companies']));
        },
        (error) => {
          this.toastrService.error("Greška prilikom editiranja dostavljača, greška: " + error);
          console.log(this.errorMessages);
        });
  }

  get f() {
    return this.editForm.controls;
  };

}
