import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryCompaniesEditComponent } from './delivery-companies-edit.component';

describe('DeliveryCompaniesEditComponent', () => {
  let component: DeliveryCompaniesEditComponent;
  let fixture: ComponentFixture<DeliveryCompaniesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryCompaniesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryCompaniesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
