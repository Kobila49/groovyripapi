import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from "@app/auth/models";
import {UserService} from "@app/static/shared/user.service";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import { ColumnMode } from '@swimlane/ngx-datatable';
import {DeliveryCompany} from "@app/auth/models/delivery-company.model";
import {DeliveryCompanyService} from "@app/static/shared/delivery-company.service";
import {AppConstants} from "@app/common/app.constants";
import {ConfirmDialogComponent, ConfirmDialogModel} from "@app/static/menu/confirm-dialog/confirm-dialog.component";
import {delay} from "@app/shared/util/delay";

@Component({
  selector: 'rip-delivery-companies',
  templateUrl: './delivery-companies.component.html',
  styleUrls: ['./delivery-companies.component.scss']
})
export class DeliveryCompaniesComponent implements OnInit {

  @ViewChild('usersTable', {static: false}) table: any;


  rows: Array<DeliveryCompany>;
  messages: object;

  ColumnMode = ColumnMode;

  constructor(private deliveryService: DeliveryCompanyService,
              private router: Router,
              public dialog: MatDialog,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.messages = {
      emptyMessage: AppConstants.tableMessages.emptyMessage,
      totalMessage: AppConstants.tableMessages.totalMessage
    };
    this.getCompaniesList();
  }

  private getCompaniesList() {
    this.deliveryService.fetchDeliveryCompanies()
      .subscribe(
        (res) => {
          if (res) {
            // @ts-ignore
            this.rows = res.companies;
            this.rows = [...this.rows];
          }
        },
        (error) => console.log(error)
      );
  }

  createDeliveryCompany() {
    this.router.navigate([`/delivery-companies/create`]);
  }

  editCompany(row: any) {
    this.router.navigate([`/delivery-companies/edit/${row.id}`]);
  }

  deleteCompany(row: any) {
    const message = 'Želite li sigurno obrisati dostavljača?';

    const dialogData = new ConfirmDialogModel('Brisanje dostavljača', message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {

        this.deliveryService.deleteCompany(row.id).subscribe(
          () => {
            delay(2000).then(() => this.rows = this.rows.filter(m => m !== row));
            this.toastrService.success('Uspješno ste obrisali dostavljača!')
          },
          () => this.toastrService.error('Došlo je do pogreške prilikom brisanja dostavljača!')
        )
      }
    });
  }



}
