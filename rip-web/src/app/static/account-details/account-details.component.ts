import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '@app/auth/models';
import {AuthenticationService} from '@app/auth/services';
import {AppConstants} from '@app/common/app.constants';
import {ToastrService} from "ngx-toastr";
import {delay} from "@app/shared/util/delay";
import {MapService} from "@app/shared/services/map-service.service";

declare var ol: any;

@Component({
  selector: 'rip-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent implements OnInit {
  longitude: number = 15.977048;
  latitude: number = 45.813177;
  mapAccount: any;
  accountForm: FormGroup;
  user: User;
  validation: object;
  errorMessages: string[];

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private mapService: MapService,
    private toastrService: ToastrService,
    private router: Router) {
  }

  ngOnInit() {
    this.errorMessages = [];
    this.validation = {
      usernamePatternMessage: AppConstants.validationErrorMessages.usernamePattern,
      usernameMinLength: AppConstants.validation.username.minLength,
      namePatternMessage: AppConstants.validationErrorMessages.namePattern,
      nameMinLength: AppConstants.validation.name.minLength,
      surnamePatternMessage: AppConstants.validationErrorMessages.surnamePattern,
      surnameMinLength: AppConstants.validation.surname.minLength,
      passwordMinLength: AppConstants.validation.password.minLength,
      passwordPattern: AppConstants.validationErrorMessages.passwordPattern,
      emailPattern: AppConstants.validationErrorMessages.emailPattern,
      phoneNumberMinLength: AppConstants.validation.phoneNumber.minLength,
      phoneNumberPatternMessage: AppConstants.validationErrorMessages.numberPattern,
      minLengthMessage: AppConstants.validationErrorMessages.minLength,
      required: AppConstants.validationErrorMessages.required
    };
    this.mapAccount = new ol.Map({
      target: 'mapAccount',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([this.longitude, this.latitude]),
        zoom: 14
      })
    });
    this.authService.fetchUserDetails(localStorage.getItem('currentUser'))
      .subscribe(
        (response) => {
          this.user = response;
          this.createEditForm(response);
        }
      );
  }


  createForm() {
    this.accountForm = this.fb.group({
      username: ['',
        [Validators.required,
          Validators.pattern(AppConstants.validation.username.pattern),
          Validators.minLength(AppConstants.validation.username.minLength)]],
      firstName: ['',
        [Validators.required,
          Validators.pattern(AppConstants.validation.name.pattern),
          Validators.minLength(AppConstants.validation.name.minLength)]],
      lastName: ['',
        [Validators.required,
          Validators.pattern(AppConstants.validation.surname.pattern),
          Validators.minLength(AppConstants.validation.surname.minLength)]],
      address: ['', Validators.required],
      password: ['',
        [Validators.required,
          Validators.pattern(AppConstants.validation.password.pattern),
          Validators.minLength(AppConstants.validation.password.minLength)]],
      email: ['', [Validators.required, Validators.pattern(AppConstants.validation.email.pattern)]],
      phoneNumber: ['',
        [Validators.required,
          Validators.minLength(AppConstants.validation.phoneNumber.minLength),
          Validators.pattern(AppConstants.validation.phoneNumber.pattern)]]
    });
  }

  fetchPoints() {
    return this.user.points;
  }


  createEditForm(user: User) {
    this.createForm();
    this.accountForm.setValue({
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName,
      address: user.address,
      password: '',
      email: user.email,
      phoneNumber: user.phoneNumber
    });
    this.latitude=user.latitude;
    this.longitude = user.longitude;
    this.setCenter();
  }

  onSubmit() {
    if (this.accountForm.invalid) {
      for (let controlName in this.accountForm.controls) {
        this.accountForm.get(controlName)
          .markAsTouched();
      }
      return;
    }
    this.mapService.fetchCoordinates(this.accountForm.controls['address'].value).subscribe(result => {
      this.latitude = result.latitude;
      this.longitude = result.longitude;
    });
    this.errorMessages = [];
    const preparedUser = this.authService.prepareRegistrationData(this.accountForm.value);
    preparedUser.latitude = this.latitude;
    preparedUser.longitude = this.longitude;
    this.authService.modifyUser(preparedUser)
      .subscribe(
        (response) => {
          this.user = response;
          this.toastrService.success('Uspješno ste spremili promjene!');
          delay(2000).then(() => this.router.navigate(['/authentication/login']));
        },
        (error) => {
          this.toastrService.error('Dogodila se greska: ' + error);
          console.log(this.errorMessages);
        });
  }

  setCenter() {
    if (this.accountForm.controls['address'].valid) {
      this.mapService.fetchCoordinates(this.accountForm.controls['address'].value).subscribe(result => {
          if (result.latitude != null && result.longitude != null) {
            this.latitude = result.latitude;
            this.longitude = result.longitude;
            const view = this.mapAccount.getView();
            view.setCenter(ol.proj.fromLonLat([this.longitude, this.latitude]));
            view.setZoom(17);
          }
        },
        error => {
          this.toastrService.error("Ne možemo pronaći adressu.")
        }
      );
    } else {
      this.toastrService.warning("Upišite adresu.");
    }
  }

  get f() {
    return this.accountForm.controls;
  };

}
