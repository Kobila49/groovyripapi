import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MealType} from "@app/static/menu/model/meal-type.enum";
import {Meal} from "@app/static/menu/model/meal.model";
import {MenuService} from "@app/static/shared/menu-service.service";
import {Router} from "@angular/router";
import {AppConstants} from "@app/common/app.constants";
import {ToastrService} from "ngx-toastr";
import {delay} from "@app/shared/util/delay";

@Component({
  selector: 'rip-meal-form',
  templateUrl: './meal-form.component.html',
  styleUrls: ['./meal-form.component.scss']
})
export class MealFormComponent implements OnInit {

  @Input() formType: 'CREATE' | 'EDIT';

  @Input() meal: Meal;

  mealForm: FormGroup;
  mealTypes: Array<MealType>;
  validation: object;
  errorMessages: string[];

  constructor(
    private fb: FormBuilder,
    private menuService: MenuService,
    private router: Router,
    private toastrService: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.errorMessages = [];
    this.validation = {
      required: AppConstants.validationErrorMessages.required
    };
    this.mealTypes = [MealType.BURGER, MealType.KEBAB, MealType.MEXICAN, MealType.PIZZA, MealType.OTHER];
    if (this.formType === 'CREATE') {
      this.createForm();
    } else if (this.formType === 'EDIT') {
      this.createEditForm(this.meal);
    } else {
      throw Error('UNSUPPORTED FORM TYPE');
    }
  }

  createForm() {
    this.mealForm = this.fb.group({
      name: ['',
        [Validators.required]],
      type: ['',
        [Validators.required,]],
      price: ['',
        [Validators.required,]]
    });
  }

  createEditForm(meal: Meal) {
    this.createForm();
    this.mealForm.setValue({
      name: meal.name,
      type: meal.type,
      price: meal.price
    });
  }

  onSubmit() {
    if (this.mealForm.invalid) {
      for (let controlName in this.mealForm.controls) {
        this.mealForm.get(controlName)
          .markAsTouched();
      }
      ;
      return;
    }
    this.errorMessages = [];

    const preparedMealData = this.menuService.prepareMealData(this.mealForm.value);

    if (this.formType === 'CREATE') {
      this.createNewMeal(preparedMealData);
    } else if (this.formType === 'EDIT') {
      this.editMeal(preparedMealData);
    } else {
      throw Error('UNSUPPORTED FORM TYPE');
    }
  }

  private createNewMeal(preparedMealData: Meal) {
    this.menuService.createMeal(preparedMealData)
      .subscribe(
        (response) => {
          this.meal = response;
          this.toastrService.success('Uspješno ste spremili jelo!');
          delay(2000).then(() => this.router.navigate(['/menu']));
        },
        (error) => {
          this.toastrService.error("Greška prilikom spremanja jela, greška: "+ error)
        });
  }

  private editMeal(preparedMealData: Meal) {
    this.menuService.modifyMeal(this.meal.id, preparedMealData)
      .subscribe(
        (response) => {
          this.meal = response;
          this.toastrService.success('Uspješno ste uredili jelo!');
          delay(2000).then(() => this.router.navigate(['/menu']));
        },
        (error) => {
          this.toastrService.error("Greška prilikom editiranja jela, greška: "+ error)
        });
  }

  get f() {
    return this.mealForm.controls;
  };
}
