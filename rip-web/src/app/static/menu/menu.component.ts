import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {routerTransition} from '@app/core';
import {AppConstants} from '@app/common/app.constants';
import {OrderResource} from '@app/static/menu/model/order-resource';
import {Meal} from '@app/static/menu/model/meal.model';
import {MenuService} from '@app/static/shared/menu-service.service';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@app/static/menu/confirm-dialog/confirm-dialog.component';
import {AuthenticationService} from '@app/auth/services';
import {User} from '@app/auth/models';
import {ToastrService} from "ngx-toastr";
import {DeliveryCompany} from "@app/auth/models/delivery-company.model";
import {DeliveryCompanyService} from "@app/static/shared/delivery-company.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {switchMap} from "rxjs/operators";


@Component({
  selector: 'rip-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [routerTransition]
})
export class MenuComponent implements OnInit {

  @ViewChild('ordersTable', {static: false}) table: any;

  user: User;
  deliveryCompanies: Array<DeliveryCompany>;
  deliveryCompany: DeliveryCompany;

  deliveryForm: FormGroup;
  rows: Array<Meal>;
  messages: object;
  order: Array<Meal>;
  itemList: OrderResource;
  totalAmount: number;
  totalTime: number;
  IsChecked: boolean;
  IsIndeterminate: boolean;
  LabelAlign: string;
  userType: boolean;
  errorMessages: string[];
  validation: object;


  constructor(private menuService: MenuService,
              private router: Router,
              private fb: FormBuilder,
              public dialog: MatDialog,
              private toastrService: ToastrService,
              private companyService: DeliveryCompanyService,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.IsChecked = false;
    this.order = [];
    this.errorMessages = [];
    this.itemList = new OrderResource();
    this.itemList.itemList = [];
    this.messages = {
      emptyMessage: AppConstants.tableMessages.emptyMessage,
      totalMessage: AppConstants.tableMessages.totalMessage
    };
    this.validation = {
      required: AppConstants.validationErrorMessages.required
    };
    this.fetchData();
    this.getMealList();
    this.userType = localStorage.getItem('currentUserRole').toString() == 'ROLE_USER';
  }


  private fetchData() {
    this.authService.fetchUserDetails(localStorage.getItem('currentUser'))
      .pipe(
        switchMap(user => {
            this.user = user;
            return this.companyService.calculateCompanies(user.longitude, user.latitude);
          }
        )).subscribe((companies) => {
      this.deliveryCompanies = companies;
      this.createCompanyForm();
    });
  }

  OnChange(event) {
    this.IsChecked = !this.IsChecked;
    if (this.IsChecked) {
      this.totalAmount *= 0.8;
    } else {
      this.totalAmount = this.calculateTotalAmount();
    }
  }

  OnIndeterminateChange($event) {

  }

  public getMealList() {
    this.menuService.fetchMealList()
      .subscribe(
        (res) => {
          if (res.meals) {
            this.rows = res.meals;
            this.rows = [...this.rows];
          }
        },
        (error) => console.log(error)
      );
  }

  addToBucket(row: any) {
    this.order.push(row);
    this.totalAmount = this.calculateTotalAmount();
    this.IsChecked = false;
  }

  clearBucket() {
    this.order = [];
  }

  deleteFromBucket(index: number) {
    this.order.splice(index, 1);
    this.totalAmount = this.calculateTotalAmount();
    this.IsChecked = false;
  }

  makeOrder() {
    if (this.deliveryForm.invalid) {
      for (let controlName in this.deliveryForm.controls) {
        this.deliveryForm.get(controlName)
          .markAsTouched();
      }
      return;
    }
    this.errorMessages = [];

    for (const o of this.order) {
      this.itemList.itemList.push(o.id);
    }
    this.itemList.discount = this.IsChecked;
    this.itemList.deliveryPrice = this.deliveryCompany.totalPrice;
    this.itemList.deliveryTime = this.deliveryCompany.totalTime;

    const message = 'Želite li povrditi narudžbu?';

    const dialogData = new ConfirmDialogModel('Potvrdite narudžbu', message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.menuService.makeOrder(this.itemList)
          .subscribe(
            (response) => {
              this.router.navigate(['/orders/' + localStorage.getItem('currentUser')]);
            },
            (err) => {
              console.log(err);
            }
          );
      }
    });
  }

  calculateTotalAmount() {
    let pom = 0;
    for (const o of this.order) {
      pom += o.price;
    }
    if (this.deliveryCompany != null) {
      pom = pom + this.deliveryCompany.totalPrice;
      this.totalTime = this.deliveryCompany.time;
    }
    return pom;
  }

  onDetailToggle(event: any) {
    console.log('Detail Toggled', event);
  }

  toggleExpandGroup(group) {
    console.log('Toggled Expand Group!', group);
    this.table.groupHeader.toggleExpandGroup(group);
  }

  deleteMeal(meal: Meal) {
    this.rows = this.rows.filter(m => m !== meal);
    this.menuService.deleteMeal(meal.id).subscribe(
      () => this.toastrService.success('Uspješno ste obrisali jelo!'),
      () => this.toastrService.success('Došlo je do pogreške prilikom brisanja jela!')
    )
  }

  editMeal(row) {
    this.router.navigate([`/menu/edit/${row.id}`]);
  }

  createMeal() {
    this.router.navigate([`/menu/create`]);
  }

  private createCompanyForm() {
    this.deliveryForm = this.fb.group({
      deliveryCompany: ['', Validators.required]
    });
  }


  get f() {
    return this.deliveryForm.controls;
  };

  changedDelivery(company: DeliveryCompany) {
    this.deliveryCompany = company;
    this.totalAmount = this.calculateTotalAmount()
  }
}
