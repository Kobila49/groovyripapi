import {Component, OnInit} from '@angular/core';
import {Meal} from "@app/static/menu/model/meal.model";
import {ActivatedRoute} from "@angular/router";
import {MenuService} from "@app/static/shared/menu-service.service";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'rip-edit-meal',
  templateUrl: './edit-meal.component.html',
  styleUrls: ['./edit-meal.component.scss']
})
export class EditMealComponent implements OnInit {

  meal: Meal;

  constructor(
    private menuService: MenuService,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => {
          const id = params.get('id');
          return this.menuService.fetchMeal(id);
        }
      )
    ).subscribe((meal: Meal) => {
      this.meal = meal;
    });
  }
}
