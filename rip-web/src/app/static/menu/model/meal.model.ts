import {MealType} from "@app/static/menu/model/meal-type.enum";

export class Meal {
  id: number;
  name: string;
  type: MealType;
  price: number;
}
