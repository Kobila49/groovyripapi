export enum MealType {
  BURGER = "BURGER", KEBAB = "KEBAB", MEXICAN = "MEXICAN", PIZZA = "PIZZA", OTHER = "OTHER"
}
