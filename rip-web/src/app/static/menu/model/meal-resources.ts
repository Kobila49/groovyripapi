import {Meal} from '@app/static/menu/model/meal.model';

export class MealResources {
  meals: Array<Meal>;

}
