import {OrderItem} from '@app/static/account-orders/model/orderItem.model';

export class OrderResource {
  itemList: Array<number>;
  discount: boolean;
  deliveryPrice: number;
  deliveryTime: number;
}
