import {Component, OnInit} from '@angular/core';
import {Meal} from "@app/static/menu/model/meal.model";

@Component({
  selector: 'rip-create-meal',
  templateUrl: './create-meal.component.html',
  styleUrls: ['./create-meal.component.scss']
})
export class CreateMealComponent implements OnInit {

  meal: Meal;

  constructor() {
  }

  ngOnInit(): void {
    this.meal = new Meal();
  }
}
