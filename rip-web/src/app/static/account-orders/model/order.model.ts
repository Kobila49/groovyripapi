import {User} from '@app/auth/models';
import {OrderItem} from '@app/static/account-orders/model/orderItem.model';

export class Order {
  createdAt: Date;
  orderItems: Array<OrderItem>;
  totalTime: number;
  totalPrice: number;
  discount: boolean;
  price: number;
  user: User;
  uuid: string;
}
