import {Order} from "@app/static/account-orders/model/order.model";

export class OrderListResource {
  orders: Array<Order>;

}
