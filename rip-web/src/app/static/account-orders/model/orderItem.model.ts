export class OrderItem {
  mealId: number;
  mealName: string;
  mealType: string;
  mealPrice: number;
}
