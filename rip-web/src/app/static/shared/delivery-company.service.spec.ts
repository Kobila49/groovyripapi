import { TestBed } from '@angular/core/testing';

import { DeliveryCompany.ServiceService } from './delivery-company.service.service';

describe('DeliveryCompany.ServiceService', () => {
  let service: DeliveryCompany.ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeliveryCompany.ServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
