import {Injectable} from '@angular/core';
import {environment} from "@env/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {DeliveryCompany} from "@app/auth/models/delivery-company.model";
import {catchError, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {User} from "@app/auth/models";

@Injectable({
  providedIn: 'root'
})
export class DeliveryCompanyService {
  private baseUrl = `${environment.ripApiUrl}` + '/api/web/delivery/companies';


  constructor(private httpClient: HttpClient) {
  }

  fetchDeliveryCompanies(): Observable<Array<DeliveryCompany>> {
    const url = this.baseUrl;
    return this.httpClient.get<Array<DeliveryCompany>>(url);
  }

  createCompany(deliveryCompany: DeliveryCompany): Observable<DeliveryCompany> {
    return this.httpClient.post<DeliveryCompany>(this.baseUrl, deliveryCompany)
      .pipe(
        tap((company) => console.log(`created delivery company with name=${company.name}`),
          error => this.handleError<DeliveryCompany>(`createDeliveryCompany`))
      );
  }

  editUser(deliveryCompany: DeliveryCompany, id: number) {
    return this.httpClient.put<DeliveryCompany>(this.baseUrl + '/' + id, deliveryCompany)
      .pipe(
        tap((company) => console.log(`edited company with name=${company.name}`),
          error => this.handleError<User>(`editDeliveryCompany`))
      );
  }

  deleteCompany(company: DeliveryCompany | number): Observable<any> {
    const id = typeof company === 'number' ? company : company.id;
    const url = this.baseUrl + "/" + id;
    // @ts-ignore
    return this.httpClient.delete<any>(url).pipe(
      tap(_ => console.log(`deleted company id=${id}`),
        error => this.handleError<any>(`deleteCOmpany`))
    )
  }

  prepareCompany(formData: any): DeliveryCompany {
    const formModel = formData;
    const data: DeliveryCompany = new DeliveryCompany();
    data.name = formModel.name;
    data.price = formModel.price
    data.time = formModel.time;

    return data;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation);
      console.error(error);
      return throwError(result as T);
    };
  }

  fetchCompanyById(id: string) {
    const url = this.baseUrl + "/" + id;
    // @ts-ignore
    return this.httpClient.get<DeliveryCompany>(url).pipe(
      tap(_ => console.log(`fetched user id=${id}`)),
      catchError(this.handleError<DeliveryCompany>('fetchUserById'))
    )
  }

  calculateCompanies(longitude: number, latitude: number): Observable<Array<DeliveryCompany>> {
    let url = this.baseUrl + '/calculate';
    return this.httpClient.post<Array<DeliveryCompany>>(url, {longitude: longitude, latitude: latitude})
      .pipe(
        tap((company) => console.log(`fetched delivery companies calculations`),
          error => this.handleError<Array<DeliveryCompany>>(`fetchDeliveryCalculations`))
      );
  }
}
