import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Order} from '@app/static/account-orders/model/order.model';
import {OrderResource} from '@app/static/menu/model/order-resource';
import {MealResources} from '@app/static/menu/model/meal-resources';
import {environment} from '@env/environment';
import {Observable} from 'rxjs/internal/Observable';
import {Meal} from "@app/static/menu/model/meal.model";
import {tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {MealType} from "@app/static/menu/model/meal-type.enum";

@Injectable({providedIn: 'root'})
export class MenuService {
  private mealsUrl = '/meals';
  private baseUrl = `${environment.ripApiUrl}` + '/api/web';

  constructor(private httpClient: HttpClient) {
  }

  fetchMealList(): Observable<MealResources> {
    const url = this.baseUrl + this.mealsUrl;
    return this.httpClient.get<MealResources>(url);
  }

  makeOrder(order: OrderResource): Observable<Order> {
    const url = this.baseUrl + '/' + localStorage.getItem('currentUser') + '/orders';
    return this.httpClient.post<Order>(url, order);
  }

  createMeal(preparedMealData: Meal): Observable<Meal> {
    const url = this.baseUrl + this.mealsUrl;
    return this.httpClient.post<Meal>(url, preparedMealData)
      .pipe(
        tap((newMeal) => console.log(`created meal with name=${newMeal.name}`),
          error => this.handleError<Meal>(`createMeal`))
      );
  }

  modifyMeal(id: any, preparedMealData: Meal): Observable<Meal> {
    const url = this.baseUrl + this.mealsUrl + "/" + id;
    return this.httpClient.put<Meal>(url, preparedMealData)
      .pipe(
        tap((newMeal) => console.log(`edited meal with id=${newMeal.id}`),
          error => this.handleError<Meal>(`edit meal id=${id}`))
      );
  }

  fetchMeal(id: any): Observable<Meal> {
    const url = this.baseUrl + this.mealsUrl + "/" + id;
    return this.httpClient.get<Meal>(url)
      .pipe(
        tap(_ => console.log(`fetched meal id=${id}`),
          error => this.handleError<Meal>(`fetchMeal id=${id}`))
      );
  }

  deleteMeal(meal: Meal | number): Observable<Meal> {
    const id = typeof meal === 'number' ? meal : meal.id;
    const url = this.baseUrl + this.mealsUrl + "/" + id;
    return this.httpClient.delete<Meal>(url).pipe(
      tap(
        _ => console.log(`deleted meal id=${id}`),
        error => this.handleError<Meal>(`deleteMeal id=${id}`))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation);
      console.error(error);
      return throwError(result as T);
    };
  }

  prepareMealData(formData: any) {
    const formModel = formData;
    const data: Meal = new Meal();
    data.name = formModel.name
    data.type = MealType[formModel.type]
    data.price = formModel.price

    return data;
  }

}
