import {Injectable} from '@angular/core';
import {environment} from "@env/environment";
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {User} from "@app/auth/models";
import {UserResources} from "@app/static/users/model/user-resources";
import {catchError, tap} from "rxjs/operators";
import {RoleResources} from "@app/static/users/model/role-resources";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = `${environment.ripApiUrl}` + '/api/users';

  constructor(private httpClient: HttpClient) {
  }

  fetchUsersList(): Observable<UserResources> {
    return this.httpClient.get<UserResources>(this.baseUrl);
  }

  fetchUserById(id: string): Observable<User> {
    const url = this.baseUrl + "/details/" + id;
    // @ts-ignore
    return this.httpClient.get<User>(url).pipe(
      tap(_ => console.log(`fetched user id=${id}`)),
      catchError(this.handleError<User>('fetchUserById'))
    )
  }

  fetchPossibleRoles(): Observable<RoleResources> {
    const rolesUrl = this.baseUrl + '/roles';
    return this.httpClient.get<RoleResources>(rolesUrl)
  }

  prepareUser(formData: any): User {
    const formModel = formData;
    const data: User = new User();
    data.username = formModel.username;
    data.role = formModel.role
    data.firstName = formModel.firstName;
    data.lastName = formModel.lastName;
    data.address = formModel.address;
    data.password = formModel.password;
    data.email = formModel.email;
    data.phoneNumber = formModel.phoneNumber;

    return data;
  }

  prepareEditedUser(formData: any): User {
    const formModel = formData;
    const data: User = new User();
    data.username = formModel.username;
    data.role = formModel.role
    data.firstName = formModel.firstName;
    data.lastName = formModel.lastName;
    data.address = formModel.address;
    data.email = formModel.email;
    data.phoneNumber = formModel.phoneNumber;
    data.points = formModel.points;
    data.role = 1
    data.password = "PASSWORD_NOT_REQUIRED"

    return data;
  }

  createUser(preparedUser: User): Observable<User> {
    return this.httpClient.post<User>(this.baseUrl + '/create', preparedUser)
      .pipe(
        tap((user) => console.log(`created user with username=${user.username}`),
          error => this.handleError<User>(`createUser`))
      );
  }

  editUser(preparedUser: User, id: number) {
    return this.httpClient.put<User>(this.baseUrl + '/edit/' + id, preparedUser)
      .pipe(
        tap((user) => console.log(`edited user with username=${user.username}`),
          error => this.handleError<User>(`editUser`))
      );
  }

  deleteUser(user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = this.baseUrl + "/" + id;
    // @ts-ignore
    return this.httpClient.delete<User>(url).pipe(
      tap((user) => console.log(`deleted user id=${id}`),
        error => this.handleError<User>(`deleteUser`))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation);
      console.error(error);
      return throwError(result as T);
    };
  }
}
