import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@app/auth/guards';
import {AccountDetailsComponent} from '@app/static/account-details/account-details.component';
import {AccountOrdersComponent} from '@app/static/account-orders/account-orders.component';
import {HomeComponent} from '@app/static/home/home.component';
import {MenuComponent} from '@app/static/menu/menu.component';
import {WorkingHoursComponent} from '@app/static/working-hours/working-hours.component';
import {EditMealComponent} from "@app/static/menu/edit-meal/edit-meal.component";
import {CreateMealComponent} from "@app/static/menu/create-meal/create-meal.component";
import {UsersComponent} from "@app/static/users/users.component";
import {CreateUserComponent} from "@app/static/users/create-user/create-user.component";
import {EditUserComponent} from "@app/static/users/edit-user/edit-user.component";
import {DeliveryCompaniesComponent} from "@app/static/delivery-companies/delivery-companies.component";
import {DeliveryCompaniesAddComponent} from "@app/static/delivery-companies/delivery-companies-add/delivery-companies-add.component";
import {DeliveryCompaniesEditComponent} from "@app/static/delivery-companies/delivery-companies-edit/delivery-companies-edit.component";


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: {title: 'O nama'}
  },
  {
    path: 'working-hours',
    component: WorkingHoursComponent,
    data: {title: 'Radno vrijeme'}
  },
  {
    path: 'menu',
    component: MenuComponent,
    canActivate: [AuthGuard],
    data: {title: 'Meni'}
  },
  {
    path: 'menu/create',
    component: CreateMealComponent,
    canActivate: [AuthGuard],
    data: {title: 'Dodaj jelo'}
  },
  {
    path: 'menu/edit/:id',
    component: EditMealComponent,
    canActivate: [AuthGuard],
    data: {title: 'Uredi jelo'}
  },
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: {title: 'Korisnici'}
  },
  {
    path: 'users/create',
    component: CreateUserComponent,
    canActivate: [AuthGuard],
    data: {title: 'Dodaj korisnika'}
  },
  {
    path: 'users/edit/:id',
    component: EditUserComponent,
    canActivate: [AuthGuard],
    data: {title: 'Uredi korisnika'}
  },

  {
    path: 'details/:id',
    component: AccountDetailsComponent,
    canActivate: [AuthGuard],
    data: {title: 'Moj profil'}
  },
  {
    path: 'orders/:id',
    component: AccountOrdersComponent,
    canActivate: [AuthGuard],
    data: {title: 'Moje narudžbe'}
  },
  {
    path: 'delivery-companies',
    component: DeliveryCompaniesComponent,
    canActivate: [AuthGuard],
    data: {title: 'Dostavljači'}
  },
  {
    path: 'delivery-companies/create',
    component: DeliveryCompaniesAddComponent,
    canActivate: [AuthGuard],
    data: {title: 'Dodaj dostavljača'}
  },
  {
    path: 'delivery-companies/edit/:id',
    component: DeliveryCompaniesEditComponent,
    canActivate: [AuthGuard],
    data: {title: 'Uredi dostavljača'}
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule {
}
