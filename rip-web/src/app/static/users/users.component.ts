import {Component, OnInit, ViewChild} from '@angular/core';
import {ColumnMode} from "@swimlane/ngx-datatable";
import {User} from "@app/auth/models";
import {UserService} from "@app/static/shared/user.service";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import {AppConstants} from "@app/common/app.constants";
import {ConfirmDialogComponent, ConfirmDialogModel} from "@app/static/menu/confirm-dialog/confirm-dialog.component";
import {delay} from "@app/shared/util/delay";


@Component({
  selector: 'rip-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @ViewChild('usersTable', {static: false}) table: any;

  rows: Array<User>;
  messages: object;

  ColumnMode = ColumnMode;

  constructor(private userService: UserService,
              private router: Router,
              public dialog: MatDialog,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.messages = {
      emptyMessage: AppConstants.tableMessages.emptyMessage,
      totalMessage: AppConstants.tableMessages.totalMessage
    };
    this.getUserList();
  }

  public getUserList() {
    this.userService.fetchUsersList()
      .subscribe(
        (res) => {
          if (res) {
            this.rows = res.users.filter(user => user.username !== localStorage.getItem('currentUser'));
            this.rows = [...this.rows];
          }
        },
        (error) => console.log(error)
      );
  }

  createUser() {
    this.router.navigate([`/users/create`]);
  }

  deleteUser(row) {
    const message = 'Želite li sigurno obrisati korisnika?';

    const dialogData = new ConfirmDialogModel('Brisanje korisnika', message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {

        this.userService.deleteUser(row.id).subscribe(
          () => {
            delay(2000).then(() => this.rows = this.rows.filter(m => m !== row));
            this.toastrService.success('Uspješno ste obrisali korisnika!')
          },
          () => this.toastrService.success('Došlo je do pogreške prilikom brisanja korisnika!')
        )
      }
    });

  }

  editUser(row) {
    this.router.navigate([`/users/edit/${row.id}`]);
  }
}
