import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "@app/auth/models";
import {Router} from "@angular/router";
import {AppConstants} from "@app/common/app.constants";
import {UserService} from "@app/static/shared/user.service";
import {Role} from "@app/static/users/model/role.model";
import {delay} from "@app/shared/util/delay";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'rip-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  create: FormGroup;
  user: User;
  roles: Array<Role>
  validation: object;
  errorMessages: string[];

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private toastrService: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.errorMessages = [];
    this.userService.fetchPossibleRoles().subscribe((resource) =>
      this.roles = resource.roles.filter(role => role.name !== 'ROLE_USER'));
    this.validation = {
      usernamePatternMessage: AppConstants.validationErrorMessages.usernamePattern,
      usernameMinLength: AppConstants.validation.username.minLength,
      passwordMinLength: AppConstants.validation.password.minLength,
      passwordPattern: AppConstants.validationErrorMessages.passwordPattern,
      emailPattern: AppConstants.validationErrorMessages.emailPattern,
      minLengthMessage: AppConstants.validationErrorMessages.minLength,
      required: AppConstants.validationErrorMessages.required
    };
    this.createForm();
  }

  createForm() {
    this.create = this.fb.group({
      username: ['',
        [Validators.required,
          Validators.pattern(AppConstants.validation.username.pattern),
          Validators.minLength(AppConstants.validation.username.minLength)]],
      role: ['', Validators.required],
      firstName: ['',],
      lastName: ['',],
      address: ['',],
      phoneNumber: ['',],
      password: ['',
        [Validators.required,
          Validators.pattern(AppConstants.validation.password.pattern),
          Validators.minLength(AppConstants.validation.password.minLength)]],
      email: ['', [Validators.required, Validators.pattern(AppConstants.validation.email.pattern)]]
    });
  }

  onSubmit() {
    if (this.create.invalid) {
      for (let controlName in this.create.controls) {
        this.create.get(controlName)
          .markAsTouched();
      }
      return;
    }
    this.errorMessages = [];
    const preparedUser = this.userService.prepareUser(this.create.value);
    this.userService.createUser(preparedUser)
      .subscribe(
        (response) => {
          this.user = response;
          this.toastrService.success('Uspješno ste spremili korisnika!');
          delay(2000).then(() => this.router.navigate(['/users']));
        },
        (error) => {
          this.toastrService.error("Greška prilikom spremanja korisnika, greška: "+ error);
          console.log(this.errorMessages);
        });
  }

  get f() {
    return this.create.controls;
  };

}
