import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "@app/auth/models";
import {Role} from "@app/static/users/model/role.model";
import {UserService} from "@app/static/shared/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AppConstants} from "@app/common/app.constants";
import {switchMap} from "rxjs/operators";
import {delay} from "@app/shared/util/delay";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'rip-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  editForm: FormGroup;
  role: Role;
  disabled: boolean;
  user: User;
  roles: Array<Role>
  validation: object;
  errorMessages: string[];

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private toastrService: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.errorMessages = [];
    this.validation = {
      usernamePatternMessage: AppConstants.validationErrorMessages.usernamePattern,
      usernameMinLength: AppConstants.validation.username.minLength,
      emailPattern: AppConstants.validationErrorMessages.emailPattern,
      minLengthMessage: AppConstants.validationErrorMessages.minLength,
      required: AppConstants.validationErrorMessages.required
    };
    this.route.paramMap.pipe(
      switchMap(params => {
          const id = params.get('id');
          return this.userService.fetchUserById(id);
        }
      )
    ).subscribe((user: User) => {
      this.user = user;
      this.disabled = 'ROLE_USER' == user.role;
      this.createEditForm(user);
    });
  }

  createForm() {
    this.editForm = this.fb.group({
      username: ['',
        [Validators.required,
          Validators.pattern(AppConstants.validation.username.pattern),
          Validators.minLength(AppConstants.validation.username.minLength)]],
      role: [{value: '', disabled: true}],
      firstName: ['',],
      lastName: ['',],
      address: ['',],
      phoneNumber: ['',],
      points: [{value: '', disabled: !this.disabled}],
      email: ['', [Validators.required, Validators.pattern(AppConstants.validation.email.pattern)]]
    });
  }

  createEditForm(user: User) {
    this.createForm();
    this.editForm.setValue({
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName,
      points: user.points,
      role: user.role,
      address: user.address,
      email: user.email,
      phoneNumber: user.phoneNumber
    });
  }

  onSubmit() {
    if (this.editForm.invalid) {
      for (let controlName in this.editForm.controls) {
        this.editForm.get(controlName)
          .markAsTouched();
      }
      return;
    }
    this.errorMessages = [];
    const preparedUser = this.userService.prepareEditedUser(this.editForm.value);
    this.userService.editUser(preparedUser, this.user.id)
      .subscribe(
        (response) => {
          this.user = response;
          this.toastrService.success('Uspješno ste uredili korisnika!');
          delay(2000).then(() => this.router.navigate(['/users']));
        },
        (error) => {
          this.toastrService.error("Greška prilikom editiranja korisnika, greška: " + error);
          console.log(this.errorMessages);
        });
  }

  get f() {
    return this.editForm.controls;
  };

}
