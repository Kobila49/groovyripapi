import {User} from "@app/auth/models";

export class UserResources {
  users: Array<User>;
}
