import {Role} from "@app/static/users/model/role.model";

export class RoleResources {
  roles: Array<Role>;
}
