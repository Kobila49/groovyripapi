export class WorkingDay {
  day: string;
  time: string;

  constructor(day: string, value: string) {
    this.day = day;
    this.time = value;
  }

}
