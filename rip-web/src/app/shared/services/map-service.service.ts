import {Injectable} from "@angular/core";
import {environment} from "@env/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {tap} from "rxjs/operators";
import {Meal} from "@app/static/menu/model/meal.model";

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private baseUrl: string = `${environment.ripApiUrl}/api/util/map`;

  constructor(private httpClient: HttpClient) {
  }

  fetchCoordinates(address: string): Observable<any> {
    const url = this.baseUrl + "/coordinates";
    return this.httpClient.post<any>(url, {address: address});
  }

}
