import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Principal, User} from '@app/auth/models';

import {environment} from '@env/environment';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Observable} from 'rxjs/internal/Observable';
import {map, tap} from 'rxjs/operators';
import {throwError} from "rxjs";

@Injectable()
export class AuthenticationService {
  readonly rootUrl = `${environment.ripApiUrl}`;
  readonly userUrl = this.rootUrl + '/api/auth/login';


  private authSource = new BehaviorSubject<string>(localStorage.getItem('isAuthenticated'));
  currentAuth = this.authSource.asObservable();


  constructor(public http: HttpClient,
              private router: Router) {
  }

  getToken() {
    return localStorage.getItem('access_token');
  }


  loginUser(username: string, password: string): Observable<any> {
    return this.http.post<any>(this.userUrl, {username: username, password: password}, {observe: 'response'})
      .pipe(map((res: any) => {
        if (res) {
          // store username and jwt token in local storage to keep user logged in between page refreshes
          const authToken = res.body.type + " " + res.body.token;
          const user = res.body.username;
          localStorage.setItem('currentUser', user);
          localStorage.setItem('access_token', authToken);
          localStorage.setItem('currentUserRole', res.body.roles);
          this.setAuthStatus('true');
        }
      }));
  }

  fetchUserDetails(username: String): Observable<any> {
    return this.http.get<any>(this.rootUrl + '/api/users/' + username);
  }

  modifyUser(user: User): Observable<User> {
    const url = this.rootUrl + '/api/users/modify/' + localStorage.getItem('currentUser');
    return this.http.put<User>(url, user)
      .pipe(
        tap((user) => console.log(`modified user with id=${user.id}`),
          error => this.handleError<User>(`modifyAccount`))
      );
  }

  registerUser(user: User) {
    return this.http.post<any>(this.rootUrl + '/api/auth/register', user)
      .pipe(
        tap((_) => console.log(`register user`),
          error => this.handleError<User>(`registerAccount`))
      );
  }

  preparePrincipalData(formData: any): Principal {
    const formModel = formData;
    const data: Principal = new Principal();
    data.username = formModel.username;
    data.password = formModel.password;

    return data;
  }

  prepareRegistrationData(formData: any): User {
    const formModel = formData;
    const data: User = new User();
    data.username = formModel.username;
    data.firstName = formModel.firstName;
    data.lastName = formModel.lastName;
    data.address = formModel.address;
    data.password = formModel.password;
    data.email = formModel.email;
    data.phoneNumber = formModel.phoneNumber;

    return data;
  }

  setAuthStatus(status: string) {
    localStorage.setItem('isAuthenticated', status);
  }

  logout(): Observable<any> {
    // remove user from local storage to log user out
    localStorage.clear();
    return this.http.get<any>(this.rootUrl + "/logout");

  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation);
      console.error(error);
      return throwError(result as T);
    };
  }

}
