export class User {
  public id: number;
  public username: string;
  public role: any;
  public firstName: string;
  public lastName: string;
  public address: string;
  public longitude: number;
  public latitude: number;
  public password: string;
  public email: string;
  public phoneNumber: string;
  public points: number;
}
