export class DeliveryCompany {
  public id: number;
  public name: string;
  public price: number;
  public time: number;
  public totalPrice: number;
  public totalTime: number;
}
