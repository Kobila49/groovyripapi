import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from "rxjs/operators";
import {AuthenticationService} from "@app/auth/services";
import {Router} from "@angular/router";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {


  constructor(private http: HttpClient,
              private authService: AuthenticationService,
              private router: Router) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let jsonReq: HttpRequest<any> = req.clone({
      setHeaders: {
        Authorization: `${localStorage.getItem('access_token')}`
      }
    });

    return next.handle(jsonReq).pipe(catchError(err => {
      if (err.status === 401) {
        this.authService.logout();
        window.location.reload(true);
        this.router.navigate(['/authentication']);
      }

      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
