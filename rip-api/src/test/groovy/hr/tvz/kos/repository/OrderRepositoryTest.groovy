package hr.tvz.kos.repository

import hr.tvz.kos.model.Order
import hr.tvz.kos.model.User
import hr.tvz.kos.support.TestBase
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Shared
import static org.assertj.core.api.Assertions.assertThat;
import java.time.ZonedDateTime

class OrderRepositoryTest extends TestBase {

    @Autowired
    OrderRepository orderRepository

    @Autowired
    UserRepository userRepository

    @Shared
    def mockOrder = createOrder(1L)

    @Shared
    def mockUser = createUser(1L)


    def setup() {
        mockUser = userRepository.save(mockUser)
        mockOrder.user = mockUser
        mockOrder = orderRepository.save(mockOrder)
    }

    def "FindByUserOrderByCreateTimeDesc"() {
        when:
        def mockOrder2 = createOrder(2)
        mockOrder2.user = userRepository.save(createUser(2))
        mockOrder2 = orderRepository.save(mockOrder2)
        def orderList = orderRepository.findByUserOrderByCreateTimeDesc(mockOrder.user)
        then:
        orderList.size() == 1
        orderList.get(0).uuid == mockOrder.uuid
    }

    def "FindByUuid"() {
        when:
        def orderFromDB = orderRepository.findByUuid(mockOrder.uuid)
        then:
        mockOrder.uuid == orderFromDB.get().uuid
        mockOrder.price == orderFromDB.get().price
    }

    Order createOrder(def number) {
        new Order(
                uuid: UUID.randomUUID().toString(),
                createTime: ZonedDateTime.now().plusHours(number),
                price: new BigDecimal("45.0"),
                deliveryTime: 5,
                deliveryPrice: 8,
                discount: false
        )
    }

    static User createUser(def number) {
        new User(
                username: "DUMMY_USERNAME_${number}",
                firstName: "DUMMY_FIRSTNAME_${number}",
                lastName: "DUMMY_LASTNAME_${number}",
                address: "DUMMY_ADDRES_${number}",
                password: "DUMMY_PASSWORD_${number}",
                email: "DUMMY_EMAIL_${number}",
                phoneNumber: "DUMMY_PHONENUMBER_${number}",
                points: 0
        )
    }
}
