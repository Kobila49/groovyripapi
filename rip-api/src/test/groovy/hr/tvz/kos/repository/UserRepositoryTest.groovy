package hr.tvz.kos.repository

import hr.tvz.kos.model.User
import hr.tvz.kos.support.TestBase
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Shared

class UserRepositoryTest extends TestBase {


    @Autowired
    UserRepository userRepository

    @Shared
    def mockUser = createUser()

    def setup() {
        mockUser = userRepository.save(mockUser)
    }

    def "FindByUsername"() {
        when:
        def userFromDatabase = userRepository.findByUsername(mockUser.username)

        then:
        mockUser.username == userFromDatabase.get().username

    }

    def "ExistsByUsername"() {
        when:
        def booleanResult = userRepository.existsByUsername(mockUser.username)

        then:
        booleanResult
    }

    def "ExistsByEmailIgnoreCase"() {
        when:
        def booleanResult = userRepository.existsByEmailIgnoreCase("dummy_email")

        then:
        booleanResult
    }

    static User createUser() {
        new User(
                username: "DUMMY_USERNAME",
                firstName: "DUMMY_FIRSTNAME",
                lastName: "DUMMY_LASTNAME",
                address: "DUMMY_ADDRES",
                password: "DUMMY_PASSWORD",
                email: "DUMMY_EMAIL",
                phoneNumber: "DUMMY_PHONENUMBER",
                points: 0
        )
    }
}
