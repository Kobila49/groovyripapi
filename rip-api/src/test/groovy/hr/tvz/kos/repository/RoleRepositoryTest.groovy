package hr.tvz.kos.repository

import hr.tvz.kos.enums.ERole
import hr.tvz.kos.model.Role
import hr.tvz.kos.support.TestBase
import org.springframework.beans.factory.annotation.Autowired

class RoleRepositoryTest extends TestBase {

    @Autowired
    RoleRepository roleRepository

    def mockRole = createRole()

    def setup() {
        mockRole = roleRepository.save(mockRole)
    }

    def "FindByName"() {
        when:
        def role = roleRepository.findByName(ERole.ROLE_ADMIN)

        then:
        role.get().name == ERole.ROLE_ADMIN

    }

    Role createRole() {
        new Role(name: ERole.ROLE_ADMIN)
    }
}
