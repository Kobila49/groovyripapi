package hr.tvz.kos.support

import com.fasterxml.jackson.databind.ObjectMapper
import hr.tvz.kos.enums.ERole
import hr.tvz.kos.enums.MealType
import hr.tvz.kos.model.Meal
import hr.tvz.kos.model.Role
import hr.tvz.kos.model.User
import hr.tvz.kos.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Shared
import spock.lang.Specification

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity

@SpringBootTest
@ActiveProfiles("test")
class TestBase extends Specification {

    @Autowired
    DatabaseCleaner dbCleaner

    @Autowired
    UserRepository userRepository

    @Autowired
    RoleRepository roleRepository

    @Autowired
    MealRepository mealRepository;

    @Autowired
    OrderRepository orderRepository

    @Autowired
    OrderItemRepository itemRepository

    @Autowired
    PasswordEncoder encoder


    @Autowired
    protected ObjectMapper objectMapper

    @Autowired
    protected WebApplicationContext context

    @Shared
    protected MockMvc mvc


    def setup() {
        dbCleaner.clean()
        this.mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build()

    }

    protected <T> T fromJson(MvcResult result, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(result.getResponse().getContentAsString(), clazz)
    }

    protected <T> T fromJson(String content, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(content, clazz)
    }

    String toJson(Object data) throws IOException {
        return this.objectMapper.writeValueAsString(data)
    }

    User createUserWithRoleUser() {
        User user = new User(
                username: "DUMMY_USERNAME",
                firstName: "DUMMY_FIRSTNAME",
                lastName: "DUMMY_LASTNAME",
                email: "ikos1801@gmail.com",
                password: encoder.encode("DUMMY_PASSWORD"),
                address: "DUMMY_ADDRESS",
                phoneNumber: "DUMMY_PHONE_NUMBER",
                points: 0
        )
        Set<Role> roles = new HashSet<>()
        roles.add(roleRepository.findByName(ERole.ROLE_USER).get())
        user.roles = roles
        userRepository.save(user)
    }

    def createRoles() {
        roleRepository.save(new Role(name: ERole.ROLE_USER));
        roleRepository.save(new Role(name: ERole.ROLE_ADMIN));
        roleRepository.save(new Role(name: ERole.ROLE_MODERATOR));
    }

    List<Meal> createMeals() {
        List.of(
                createMeal("Chesseburger", MealType.BURGER, "24.00"),
                createMeal("Slavonska", MealType.PIZZA, "45.00"),
                createMeal("Kebab", MealType.KEBAB, "23.00")
        )
    }

    Meal createMeal(String name, MealType type, String price) {
        mealRepository.save(new Meal(name: name, mealType: type, price: new BigDecimal(price)))
    }
}
