package hr.tvz.kos.support

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import static java.util.Collections.emptyMap

@Service
class DatabaseCleaner {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Transactional
    void clean() {
        this.jdbcTemplate.update("DELETE FROM meal", emptyMap());
        this.jdbcTemplate.update("DELETE FROM order_items", emptyMap());
        this.jdbcTemplate.update("DELETE FROM orders", emptyMap());
        this.jdbcTemplate.update("DELETE FROM user_role", emptyMap());
        this.jdbcTemplate.update("DELETE FROM users", emptyMap());
        this.jdbcTemplate.update("DELETE FROM role", emptyMap());
    }
}
