package hr.tvz.kos.controller

import hr.tvz.kos.enums.ERole
import hr.tvz.kos.model.Role
import hr.tvz.kos.model.User
import hr.tvz.kos.payload.request.SignupRequest
import hr.tvz.kos.payload.request.UserRequest
import hr.tvz.kos.payload.response.RoleListResponse
import hr.tvz.kos.payload.response.UserListResponse
import hr.tvz.kos.payload.response.UserResponse
import hr.tvz.kos.security.WithMockedSecurity
import hr.tvz.kos.support.TestBase
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MvcResult

import static org.assertj.core.api.SoftAssertions.assertSoftly
import static org.assertj.core.api.Assertions.assertThat
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class UserControllerTest extends TestBase {

    def setup() {
        createRoles()
    }

    @WithMockedSecurity
    def "mustReturnUserDetails"() {
        User user = createUserWithRoleUser()

        when:
        MvcResult result = mvc.perform(get("/api/users/${user.username}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
        UserResponse response = fromJson(result, UserResponse.class)

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.username).isEqualTo(user.username)
            softly.assertThat(response.id).isEqualTo(user.id)
            softly.assertThat(response.points).isEqualTo(user.points)
        })
    }

    @WithMockedSecurity
    def "mustReturnNotFoundForNotExistingUser"() {
        expect:
        mvc.perform(get("/api/users/some_username")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())

    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustReturnUserWithRequestedId"() {
        User user = createUserWithRoleUser()

        when:
        MvcResult result = mvc.perform(get("/api/users/details/${user.id}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
        UserResponse response = fromJson(result, UserResponse.class)

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.username).isEqualTo(user.username)
            softly.assertThat(response.id).isEqualTo(user.id)
            softly.assertThat(response.points).isEqualTo(user.points)
        })
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustReturnNotFoundForUserWithNonExistentId"() {
        expect:
        mvc.perform(get("/api/users/details/-1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustModifyExistentUser"() {
        User user = createUserWithRoleUser()
        SignupRequest request = new SignupRequest(
                username: "DUMMY_USERNAME",
                firstName: "TEST_IME",
                lastName: "TEST_PREZIME",
                address: "TEST_ADRESA",
                password: "some_password",
                email: "TEST_EMAIL@RIP.COM",
                phoneNumber: "55555555"

        )
        String content = toJson(request)

        when:
        MvcResult result = mvc.perform(put("/api/users/modify/${user.username}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isAccepted())
                .andReturn()
        UserResponse response = fromJson(result, UserResponse.class)

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.username).isEqualTo(request.username)
            softly.assertThat(response.id).isEqualTo(user.id)
            softly.assertThat(response.firstName).isEqualTo(request.firstName)
            softly.assertThat(response.lastName).isEqualTo(request.lastName)
            softly.assertThat(response.email).isEqualTo(request.email)
            softly.assertThat(response.address).isEqualTo(request.address)
            softly.assertThat(response.phoneNumber).isEqualTo(request.phoneNumber)
        })
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustReturnNotFoundForModifyingNonExistentUser"() {
        SignupRequest request = new SignupRequest(
                username: "DUMMY_USERNAME",
                firstName: "TEST_IME",
                lastName: "TEST_PREZIME",
                address: "TEST_ADRESA",
                password: "some_password",
                email: "TEST_EMAIL@RIP.COM",
                phoneNumber: "55555555"

        )
        String content = toJson(request)
        expect:
        mvc.perform(put("/api/users/modify/DUMMY_USERNAME")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isNotFound())
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustReturnForbiddenForTryEditingOtherUser"() {
        SignupRequest request = new SignupRequest(
                username: "DUMMY_USERNAME",
                firstName: "TEST_IME",
                lastName: "TEST_PREZIME",
                address: "TEST_ADRESA",
                password: "some_password",
                email: "TEST_EMAIL@RIP.COM",
                phoneNumber: "55555555"

        )
        String content = toJson(request)

        expect:
        mvc.perform(put("/api/users/modify/other_user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isForbidden())
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustCreateNewUser"() {
        List<Role> roles = roleRepository.findAll();
        Role role = roles.find { it.name == ERole.ROLE_MODERATOR }
        UserRequest request = new UserRequest(
                username: "MODERATOR",
                password: "hidden_password",
                email: "somecool@email.com",
                role: role.id
        )
        String content = toJson(request)
        when:
        MvcResult result = mvc.perform(post("/api/users/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isCreated())
                .andReturn()
        UserResponse response = fromJson(result, UserResponse.class)

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.username).isEqualTo(request.username)
            softly.assertThat(response.email).isEqualTo(request.email)
            softly.assertThat(response.role).isEqualTo(role.name.name())
        })
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustReturnBadRequestForTryingToCreateUserWithExistingUsername"() {
        createUserWithRoleUser()
        List<Role> roles = roleRepository.findAll();
        Role role = roles.find { it.name == ERole.ROLE_MODERATOR }
        UserRequest request = new UserRequest(
                username: "DUMMY_USERNAME",
                password: "hidden_password",
                email: "somecool@email.com",
                role: role.id
        )
        String content = toJson(request)
        expect:
        mvc.perform(post("/api/users/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isBadRequest())
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustEditExistingUserAndReturnAccepted"() {
        User user = createUserWithRoleUser()
        List<Role> roles = roleRepository.findAll();
        Role role = roles.find { it.name == ERole.ROLE_USER }
        UserRequest request = new UserRequest(
                username: "new_username",
                password: "hidden_password",
                email: "somecool@email.com",
                address: "Hvarska 4a, Zagreb",
                role: role.id
        )
        String content = toJson(request)
        when:
        MvcResult result = mvc.perform(put("/api/users/edit/${user.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isAccepted())
                .andReturn()

        UserResponse response = fromJson(result, UserResponse.class)
        then:
        assertSoftly({ softly ->
            softly.assertThat(response.username).isEqualTo(request.username)
            softly.assertThat(response.email).isEqualTo(request.email)
        })
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustReturnBadRequestForTryingToEditToExistingUsername"() {
        List<Role> roles = roleRepository.findAll();
        Role role = roles.find { it.name == ERole.ROLE_USER }
        UserRequest request = new UserRequest(
                username: "new_username",
                password: "hidden_password",
                email: "somecool@email.com",
                role: role.id
        )
        String content = toJson(request)
        expect:
        mvc.perform(put("/api/users/edit/-1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isBadRequest())
                .andReturn()
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustReturnListOfUsers"() {
        createUserWithRoleUser()

        when:
        MvcResult result = mvc.perform(get("/api/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()

        UserListResponse response = fromJson(result, UserListResponse.class)
        then:
        assertSoftly({ softly ->
            softly.assertThat(response.users).hasSize(1)
        })
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustReturnListOfRoles"() {
        when:
        MvcResult result = mvc.perform(get("/api/users/roles")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()

        RoleListResponse response = fromJson(result, RoleListResponse.class)
        then:
        assertSoftly({ softly ->
            softly.assertThat(response.roles).hasSize(3)
        })
    }

    @WithMockedSecurity(grants = ['ROLE_ADMIN'])
    def "MustDeleteUser"() {
        User user = createUserWithRoleUser()

        when:
        mvc.perform(delete("/api/users/${user.id}"))
                .andExpect(status().isNoContent())

        then:
        Optional<User> optionalUser = userRepository.findById(user.id)
        assertThat(optionalUser).isEmpty()
    }
}