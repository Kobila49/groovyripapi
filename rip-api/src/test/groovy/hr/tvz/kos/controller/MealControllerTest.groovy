package hr.tvz.kos.controller

import hr.tvz.kos.enums.MealType
import hr.tvz.kos.model.Meal
import hr.tvz.kos.payload.request.MealRequest
import hr.tvz.kos.payload.response.MealListResponse
import hr.tvz.kos.payload.response.MealResponse
import hr.tvz.kos.security.WithMockedSecurity
import hr.tvz.kos.support.TestBase
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MvcResult

import static org.assertj.core.api.SoftAssertions.assertSoftly
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class MealControllerTest extends TestBase {


    @WithMockedSecurity
    def "MustReturnMeals"() {
        createMeals()

        when:
        MvcResult result = mvc.perform(get("/api/web/meals")
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()

        MealListResponse response = fromJson(result, MealListResponse.class);

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.meals).hasSize(3)
        })
    }

    @WithMockedSecurity(grants = ["ROLE_ADMIN"])
    def "MustReturnMealWithRequestedId"() {
        createMeals()
        Meal tempMeal = createMeal("Lazanje", MealType.OTHER, "40.00")

        when:
        MvcResult result = mvc.perform(get("/api/web/meals/" + tempMeal.id)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()

        MealResponse response = fromJson(result, MealResponse.class);

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.id).isEqualTo(tempMeal.id)
            softly.assertThat(response.name).isEqualTo(tempMeal.name)
            softly.assertThat(response.type).isEqualTo(MealType.OTHER)
            softly.assertThat(response.price).isEqualTo(tempMeal.price)
        })
    }

    @WithMockedSecurity(grants = ["ROLE_ADMIN"])
    def "MustReturnNotFoundForMealWithNonExistingId"() {
        expect:
        mvc.perform(get("/api/web/meals/-1")
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
    }

    @WithMockedSecurity
    def "MustReturnForbiddenForGetMealWithIdForUserRole"() {
        expect:
        mvc.perform(get("/api/web/meals/1")
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden())
    }

    @WithMockedSecurity(grants = ["ROLE_ADMIN"])
    def "MustCreateMeal"() {
        MealRequest meal = new MealRequest(name: "Hot dog",
                type: MealType.OTHER.name(),
                price: new BigDecimal("25"))
        String content = toJson(meal)

        when:
        MvcResult result = mvc.perform(post("/api/web/meals/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isCreated())
                .andReturn()
        MealResponse response = fromJson(result, MealResponse.class);

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.name).isEqualTo("Hot dog")
            softly.assertThat(response.type).isEqualTo(MealType.OTHER)
            softly.assertThat(response.price).isEqualTo(new BigDecimal("25"))
        })
    }

    @WithMockedSecurity(grants = ["ROLE_ADMIN"])
    def "MustEditMealWithRequestedId"() {
        Meal meal = createMeal("Cheeseburger", MealType.BURGER, "22");
        MealRequest mealRequest = new MealRequest(name: "Hot dog",
                type: MealType.OTHER.name(),
                price: new BigDecimal("25"))
        String content = toJson(mealRequest)

        when:
        MvcResult result = mvc.perform(put("/api/web/meals/" + meal.id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isAccepted())
                .andReturn()
        MealResponse response = fromJson(result, MealResponse.class);

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.name).isEqualTo("Hot dog")
            softly.assertThat(response.type).isEqualTo(MealType.OTHER)
            softly.assertThat(response.price).isEqualTo(new BigDecimal("25"))
        })
    }

    @WithMockedSecurity(grants = ["ROLE_ADMIN"])
    def "MustReturnNotFoundForEditingNonExistingMeal"() {
        MealRequest mealRequest = new MealRequest(name: "Hot dog",
                type: MealType.OTHER.name(),
                price: new BigDecimal("25"))
        String content = toJson(mealRequest)

        expect:
        mvc.perform(put("/api/web/meals/-1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isNotFound())
    }

    @WithMockedSecurity(grants = ["ROLE_ADMIN"])
    def "MustDeleteMealWithRequestedId"() {
        Meal meal = createMeal("Cheeseburger", MealType.BURGER, "22");

        expect:
        mvc.perform(delete("/api/web/meals/" + meal.id))
                .andExpect(status().isNoContent())
    }

    @WithMockedSecurity
    def "MustReturnForbidden"() {
        Meal meal = createMeal("Cheeseburger", MealType.BURGER, "22");

        expect:
        mvc.perform(delete("/api/web/meals/" + meal.id))
                .andExpect(status().isForbidden())
    }


}
