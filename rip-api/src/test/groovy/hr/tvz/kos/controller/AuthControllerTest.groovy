package hr.tvz.kos.controller


import hr.tvz.kos.model.User
import hr.tvz.kos.payload.request.LoginRequest
import hr.tvz.kos.payload.request.SignupRequest
import hr.tvz.kos.payload.response.JwtResponse
import hr.tvz.kos.support.TestBase
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MvcResult

import static org.assertj.core.api.SoftAssertions.assertSoftly
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class AuthControllerTest extends TestBase {

    def setup() {
        createRoles();
    }

    def "mustSuccessfullyAuthenticateUser"() {
        User user = createUserWithRoleUser()
        String loginRequest = toJson(new LoginRequest(username: "DUMMY_USERNAME", password: "DUMMY_PASSWORD"))

        when:
        MvcResult result = mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginRequest))
                .andExpect(status().isOk())
                .andReturn()
        JwtResponse response = fromJson(result, JwtResponse.class);

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.id).isEqualTo(user.id)
            softly.assertThat(response.username).isEqualTo(user.username)
            softly.assertThat(response.email).isEqualTo(user.email)
            softly.assertThat(response.roles).hasSize(1)
            softly.assertThat(response.roles).contains("ROLE_USER")
        })

    }

    def "mustSuccessfullyRegisterUser"() {
        String signUpRequest = toJson(createSignupRequest())
        expect:
        mvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(signUpRequest))
                .andExpect(status().isOk())
    }

    def "mustReturnBadRequestForExistingUsername"() {
        createUserWithRoleUser()
        String signUpRequest = toJson(createSignupRequest())
        expect:
        mvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(signUpRequest))
                .andExpect(status().isBadRequest())
    }


    SignupRequest createSignupRequest() {
        new SignupRequest(
                username: "DUMMY_USERNAME",
                firstName: "DUMMY_FIRSTNAME",
                lastName: "DUMMY_LASTNAME",
                email: "DUMMY_EMAIL@RIP.COM",
                password: "DUMMY_PASSWORD",
                address: "DUMMY_ADDRESS",
                phoneNumber: "DUMMY_PHONE_NUMBER"
        )
    }
}
