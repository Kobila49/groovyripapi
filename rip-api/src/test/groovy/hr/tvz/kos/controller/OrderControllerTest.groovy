package hr.tvz.kos.controller

import hr.tvz.kos.model.Meal
import hr.tvz.kos.model.Order
import hr.tvz.kos.model.OrderItem
import hr.tvz.kos.model.User
import hr.tvz.kos.payload.request.OrderRequest
import hr.tvz.kos.payload.response.OrderListResponse
import hr.tvz.kos.payload.response.OrderResponse
import hr.tvz.kos.security.WithMockedSecurity
import hr.tvz.kos.support.TestBase
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MvcResult

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import static org.assertj.core.api.SoftAssertions.assertSoftly
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class OrderControllerTest extends TestBase {

    def setup() {
        createRoles()
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustReturnUserOrders"() {
        User user = createUserWithRoleUser()
        createUserOrders(user)

        when:
        MvcResult result = mvc.perform(get("/api/web/${user.username}/orders")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
        OrderListResponse response = fromJson(result, OrderListResponse.class)

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.orders).hasSize(2)
        })
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustReturnNoContent"() {
        User user = createUserWithRoleUser()

        expect:
        mvc.perform(get("/api/web/${user.username}/orders")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
    }


    def "MustReturnUnauthorized"() {
        expect:
        mvc.perform(get("/api/web/some_username/orders")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andReturn()
    }

    @WithMockedSecurity
    def "MustReturnForbidden"() {
        expect:
        mvc.perform(get("/api/web/not_logged_username/orders")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn()
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustReturnOrderWithRequestedId"() {
        User user = createUserWithRoleUser()
        List<Order> orderList = createUserOrders(user)
        Order order = orderList.get(0)

        when:
        MvcResult result = mvc.perform(get("/api/web/orders/${order.uuid}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
        OrderResponse response = fromJson(result, OrderResponse.class)

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.uuid).isEqualTo(order.uuid)
            softly.assertThat(response.createdAt).isEqualTo(order.createTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
            softly.assertThat(response.user.username).isEqualTo(order.user.username)
            softly.assertThat(response.price).isEqualTo(order.price)
            softly.assertThat(response.discount).isEqualTo(order.discount)
        })
    }


    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustReturnNotFoundForRequestedId"() {
        expect:
        mvc.perform(get("/api/web/orders/" + "someNotExistingUuid")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustCreateRequestedOrderForUser"() {
        User user = createUserWithRoleUser()
        List<Meal> mealList = createMeals()
        OrderRequest orderRequest = new OrderRequest(
                discount: false,
                itemList: mealList*.id,
                deliveryPrice: 4,
                deliveryTime: 10
        )
        String content = toJson(orderRequest)
        when:
        MvcResult result = mvc.perform(post("/api/web/${user.username}/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk())
                .andReturn()
        OrderResponse response = fromJson(result, OrderResponse.class)

        BigDecimal sum = mealList.price.sum()
        def totalSum = sum.add(new BigDecimal("4"))

        then:
        assertSoftly({ softly ->
            softly.assertThat(response.user.username).isEqualTo(user.username)
            softly.assertThat(response.price).isEqualTo(totalSum)
            softly.assertThat(response.discount).isEqualTo(orderRequest.discount)
            softly.assertThat(response.user.points).isEqualTo(5)
            softly.assertThat(response.orderItems).hasSize(3)
        })
    }

    @WithMockedSecurity(username = "DUMMY_USERNAME")
    def "MustReturnForbiddenWhenCreateForWrongUser"() {
        User user = createUserWithRoleUser()
        List<Meal> mealList = createMeals()
        OrderRequest orderRequest = new OrderRequest(
                discount: false,
                itemList: mealList*.id,
                deliveryPrice: 4,
                deliveryTime: 10
        )
        String content = toJson(orderRequest)
        expect:
        mvc.perform(post("/api/web/wrong_username/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isForbidden())
    }

    List<Order> createUserOrders(User user) {
        List<Order> orderList = new ArrayList<>()
        for (int i = 0; i < 2; i++) {
            Order order = new Order()
            order.createTime = ZonedDateTime.now()

            // order items
            List<OrderItem> orderItems = new ArrayList<>()
            List<Meal> mealList = createMeals()
            for (int j = 0; j < mealList.size(); j++) {
                Meal meal = mealList.get(j)
                orderItems.add(new OrderItem(name: meal.name, type: meal.mealType.toString(), price: meal.price))
            }
            order.orderItems = orderItems
            order.discount = false
            order.uuid = UUID.randomUUID().toString()
            order.user = user
            order.deliveryPrice = 9
            order.deliveryTime = 7
            order.price = new BigDecimal(orderItems.price.sum() as String)
            order = orderRepository.saveAndFlush(order)
            for (OrderItem item : orderItems) {
                item.order = order
            }
            itemRepository.saveAll(orderItems)
            orderList.add(order)
        }
        orderList
    }

}
