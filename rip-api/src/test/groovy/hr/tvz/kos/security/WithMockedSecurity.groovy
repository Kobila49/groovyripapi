package hr.tvz.kos.security

import org.springframework.security.test.context.support.WithSecurityContext

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy.RUNTIME)
@Target([ElementType.METHOD])
@WithSecurityContext(factory = WithMockedSecurityContextFactory.class)
@interface WithMockedSecurity {

    String username() default "user"

    String email() default "rip@rip.com"

    String[] grants() default ["ROLE_USER"]

}