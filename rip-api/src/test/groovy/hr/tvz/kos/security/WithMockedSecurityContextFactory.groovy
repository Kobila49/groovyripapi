package hr.tvz.kos.security

import hr.tvz.kos.security.services.UserDetailsImpl
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.test.context.support.WithSecurityContextFactory

import java.util.stream.Collectors

class WithMockedSecurityContextFactory implements WithSecurityContextFactory<WithMockedSecurity> {

    @Override
    SecurityContext createSecurityContext(WithMockedSecurity annotation) {
        String username = annotation.username()
        String password = "Hidden"
        String email = annotation.email()

        List<String> grants = Arrays.asList(annotation.grants())

        List<GrantedAuthority> grantedAuthorities = grants
                .stream()
                .map({ grant -> new SimpleGrantedAuthority(grant) })
                .collect(Collectors.toList())

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                username,
                password,
                grantedAuthorities
        )

        UserDetailsImpl userDetails = new UserDetailsImpl(
                1,
                username,
                password,
                email,
                grantedAuthorities)

        token.setDetails(userDetails)
        SecurityContext context = SecurityContextHolder.createEmptyContext()
        context.setAuthentication(token)
        context
    }
}
