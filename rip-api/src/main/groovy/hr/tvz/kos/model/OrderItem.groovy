package hr.tvz.kos.model

import com.fasterxml.jackson.annotation.JsonBackReference
import groovy.transform.ToString

import javax.persistence.*

@ToString(excludes = "order")
@Entity
@Table(name = "order_items")
class OrderItem extends BaseEntity {

    @Column(name = "name", nullable = false)
    String name

    @Column(name = "type", nullable = false)
    String type

    @Column(name = "price", nullable = false)
    BigDecimal price

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", referencedColumnName = "id", nullable = false)
    @JsonBackReference(value = "item_order_reference")
    Order order


}
