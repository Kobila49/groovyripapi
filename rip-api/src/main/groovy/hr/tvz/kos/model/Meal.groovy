package hr.tvz.kos.model


import groovy.transform.ToString
import hr.tvz.kos.enums.MealType

import javax.persistence.*

@ToString(includeNames = true)
@Entity
@Table(name = "meal")
class Meal extends BaseEntity {

    @Column(nullable = false)
    String name

    @Column(nullable = false, name = "meal_type")
    @Enumerated(EnumType.STRING)
    MealType mealType

    @Column(nullable = false)
    BigDecimal price

}
