package hr.tvz.kos.model

import groovy.transform.ToString

@ToString(includeNames = true)
class Mail {

    String mailFrom
    String mailTo
    String mailCc
    String mailBcc
    String mailSubject
    String mailContent
    String contentType
    List<Object> attachments
    Map<String, Object> model

    Mail() {
        contentType = "text/plain"
    }

}
