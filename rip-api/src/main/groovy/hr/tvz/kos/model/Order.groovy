package hr.tvz.kos.model

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import groovy.transform.ToString

import javax.persistence.*
import java.time.ZonedDateTime

@ToString(includeNames = true, excludes = "user")
@Entity
@Table(name = "orders")
class Order extends BaseEntity {


    @Column(nullable = false, unique = true)
    String uuid

    @Column(nullable = false, name = "create_time")
    ZonedDateTime createTime

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @JsonBackReference(value = "order_user_reference")
    User user

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    @JsonManagedReference(value = "item_order_reference")
    List<OrderItem> orderItems

    @Column(name = "price", nullable = false)
    BigDecimal price

    @Column(name = "delivery_price", nullable = false)
    Double deliveryPrice

    @Column(name = "delivery_time", nullable = false)
    Double deliveryTime

    @Column(name = "discount", nullable = false)
    Boolean discount

}

