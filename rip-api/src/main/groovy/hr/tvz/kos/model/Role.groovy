package hr.tvz.kos.model

import groovy.transform.ToString
import hr.tvz.kos.enums.ERole

import javax.persistence.*

@ToString(includeNames = true)
@Entity
@Table(name = "role")
class Role extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20, unique = true)
    ERole name

}
