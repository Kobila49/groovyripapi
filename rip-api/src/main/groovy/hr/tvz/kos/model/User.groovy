package hr.tvz.kos.model

import com.fasterxml.jackson.annotation.JsonManagedReference
import groovy.transform.ToString

import javax.persistence.*

@ToString(includeNames = true, excludes = 'orders')
@Entity
@Table(name = "users")
class User extends BaseEntity {

    @Column(nullable = false, unique = true)
    String username

    @Column(nullable = false, name = "first_name")
    String firstName

    @Column(nullable = false, name = "last_name")
    String lastName

    @Column(nullable = false)
    String address

    @Column(nullable = true)
    Double longitude

    @Column(nullable = true)
    Double latitude

    @Column(nullable = false, name = "password")
    String password

    @Column(nullable = false, name = "email", unique = true)
    String email

    @Column(nullable = false, name = "phone_number")
    String phoneNumber

    @Column(nullable = false)
    Integer points

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @JsonManagedReference(value = "order_user_reference")
    Set<Order> orders

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_account_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    Set<Role> roles

}
