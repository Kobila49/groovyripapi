package hr.tvz.kos.model

import groovy.transform.ToString

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@ToString(includeNames = true)
@Entity
@Table(name = "delivery_company")
class DeliveryCompany extends BaseEntity {

    @Column(nullable = false)
    String name

    @Column(nullable = false)
    Double price

    @Column(nullable = false)
    Double time
}
