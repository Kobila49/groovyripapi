package hr.tvz.kos.enums

enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MODERATOR
}