package hr.tvz.kos.enums

enum MealType {
    BURGER, KEBAB, MEXICAN, PIZZA, OTHER
}