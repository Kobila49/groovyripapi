package hr.tvz.kos.serviceImpl

import groovy.util.logging.Slf4j
import hr.tvz.kos.enums.ERole
import hr.tvz.kos.model.Role
import hr.tvz.kos.model.User
import hr.tvz.kos.payload.request.SignupRequest
import hr.tvz.kos.payload.request.UserRequest
import hr.tvz.kos.repository.RoleRepository
import hr.tvz.kos.repository.UserRepository
import hr.tvz.kos.service.UserService
import hr.tvz.kos.utils.OpenStreetMapUtils
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

import javax.transaction.Transactional

import static java.util.Objects.requireNonNull

@Slf4j
@Service
class UserServiceImpl implements UserService {

    final UserRepository userRepository

    final RoleRepository roleRepository

    final PasswordEncoder encoder

    UserServiceImpl(UserRepository userRepository,
                    RoleRepository roleRepository,
                    PasswordEncoder encoder) {
        this.userRepository = requireNonNull(userRepository)
        this.roleRepository = requireNonNull(roleRepository)
        this.encoder = requireNonNull(encoder)
    }

    @Override
    List<User> fetchAllUsers() {
        userRepository.findAll()
    }

    @Override
    List<Role> fetchAllRoles() {
        roleRepository.findAll()
    }

    @Override
    @Transactional
    Optional<User> createUser(UserRequest userRequest) {

        if (userRepository.existsByUsername(userRequest.username)) {
            log.warn("Username is taken")
            return Optional.empty()
        }

        if (userRepository.existsByEmailIgnoreCase(userRequest.email)) {
            log.warn("Email is already used")
            return Optional.empty()
        }

        Optional.of(saveUser(userRequest))
    }

    @Override
    Optional<User> editUser(UserRequest userRequest, Long id) {
        Optional<User> optionalUser = userRepository.findById(id)
        if (optionalUser.isEmpty()) {
            log.warn("User doesn't exist")
            return Optional.empty()
        }

        if (userRepository.existsByUsernameAndIdNot(userRequest.username, id)) {
            log.warn("Username is taken")
            return Optional.empty()
        }

        if (userRepository.existsByEmailIgnoreCaseAndIdNot(userRequest.email, id)) {
            log.warn("Email is taken")
            return Optional.empty()
        }
        Optional.of(saveExistingUser(userRequest, optionalUser.get()))
    }

    @Override
    @Transactional
    Optional<User> registerNormalUser(SignupRequest signupRequest) {

        if (userRepository.existsByUsername(signupRequest.username)) {
            log.warn("Username is taken")
            return Optional.empty()
        }

        if (userRepository.existsByEmailIgnoreCase(signupRequest.email)) {
            log.warn("Email is already used")
            return Optional.empty()
        }

        User user = saveNormalUser(signupRequest)
        Optional.of(user)
    }

    @Override
    @Transactional
    Optional<User> modifyNormalUser(SignupRequest signupRequest, String username) {
        Optional<User> optionalUser = findUserByUsername(username)
        if (optionalUser.isEmpty())
            return Optional.empty()

        modifyAndSaveUser(optionalUser.get(), signupRequest)
    }

    @Override
    Optional<User> findUserByUsername(def username) {
        userRepository.findByUsername(username)
    }

    @Override
    Optional<User> findById(Long id) {
        userRepository.findById(id)
    }

    @Override
    void deleteUser(Long id) {
        userRepository.deleteById(id)
    }

    private User saveNormalUser(SignupRequest signupRequest) {
        User user = new User(
                username: signupRequest.username,
                firstName: signupRequest.firstName,
                lastName: signupRequest.lastName,
                address: signupRequest.address,
                latitude: signupRequest.latitude,
                longitude: signupRequest.longitude,
                password: encoder.encode(signupRequest.password),
                email: signupRequest.email,
                phoneNumber: signupRequest.phoneNumber,
                points: 0
        )

        Role role = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow({ -> new RuntimeException("Error: Role is not found.") })
        Set<Role> roles = new HashSet<>()
        roles.add(role)

        user.roles = roles
        log.info("Saving user: " + user.toString())
        user = userRepository.saveAndFlush(user)
        user
    }

    private Optional<User> modifyAndSaveUser(User user, SignupRequest signupRequest) {
        user.username = signupRequest.username
        user.firstName = signupRequest.firstName
        user.lastName = signupRequest.lastName
        user.address = signupRequest.address
        user.password = encoder.encode(signupRequest.password)
        user.email = signupRequest.email
        user.phoneNumber = signupRequest.phoneNumber
        Optional.of(userRepository.saveAndFlush(user))
    }

    private User saveUser(UserRequest userRequest) {
        log.info("Creating user with data ${userRequest}")
        User user = new User(
                username: userRequest.username,
                firstName: userRequest.firstName ?: "NOT_SET",
                lastName: userRequest.lastName ?: "NOT_SET",
                address: userRequest.address ?: "NOT_SET",
                password: encoder.encode(userRequest.password),
                email: userRequest.email,
                phoneNumber: userRequest.phoneNumber ?: "NOT_SET",
                points: 0
        )

        Set<Role> roles = new HashSet<>()
        Role role = roleRepository.findById(userRequest.role)
                .orElseThrow({ -> new RuntimeException("Error: Role is not found.") })
        roles.add(role)

        user.roles = roles
        user = userRepository.saveAndFlush(user)
        log.info("User saved: " + user.toString())
        user
    }

    private User saveExistingUser(UserRequest userRequest, User user) {
        def mapCoord = OpenStreetMapUtils.getInstance().getCoordinates(userRequest.address)
        user.username = userRequest.username
        user.firstName = userRequest.firstName ?: user.firstName
        user.lastName = userRequest.lastName ?: user.lastName
        user.address = userRequest.address ?: user.address
        user.latitude = mapCoord.get('lat')
        user.longitude = mapCoord.get('lon')
        user.email = userRequest.email
        user.phoneNumber = userRequest.phoneNumber ?: user.phoneNumber
        user.points = userRequest.points ?: user.points
        userRepository.save(user)
    }
}

