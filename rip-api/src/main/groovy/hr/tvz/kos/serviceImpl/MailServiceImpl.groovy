package hr.tvz.kos.serviceImpl

import freemarker.template.Configuration
import freemarker.template.Template
import freemarker.template.TemplateException
import hr.tvz.kos.model.Mail
import hr.tvz.kos.service.MailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils

import javax.mail.MessagingException
import javax.mail.internet.MimeMessage
import java.nio.charset.StandardCharsets

@Service("mailService")
class MailServiceImpl implements MailService {

    @Autowired
    JavaMailSender mailSender

    @Autowired
    Configuration fmConfiguration

    @Override
    void sendEmail(Mail mail) throws MessagingException, IOException, TemplateException {
        fmConfiguration.setClassForTemplateLoading(this.getClass(), "/templates/")
        MimeMessage message = mailSender.createMimeMessage()
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name())

        helper.addAttachment("rip-logo.png", new ClassPathResource("rip-logo.png"))

        Template t = fmConfiguration.getTemplate("email-template.ftl")
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.model)

        helper.setTo(mail.getMailTo())
        helper.setText(html, true)
        helper.setSubject(mail.getMailSubject())
        helper.setFrom(mail.getMailFrom())

        mailSender.send(message)
    }

}
