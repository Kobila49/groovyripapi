package hr.tvz.kos.serviceImpl

import groovy.util.logging.Slf4j
import hr.tvz.kos.enums.MealType
import hr.tvz.kos.model.Meal
import hr.tvz.kos.payload.request.MealRequest
import hr.tvz.kos.repository.MealRepository
import hr.tvz.kos.service.MealService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.transaction.Transactional

@Slf4j
@Service
class MealServiceImpl implements MealService {

    @Autowired
    MealRepository repository

    @Override
    List<Meal> findAllMeals() {
        repository.findAll()
    }

    @Override
    Optional<Meal> getMealById(Long id) {
        repository.findById(id)
    }

    @Override
    Meal createMeal(MealRequest mealRequest) {
        createAndSaveMeal(mealRequest)
    }

    @Override
    Optional<Meal> modifyMeal(Long id, MealRequest mealRequest) {
        Optional<Meal> optionalMeal = repository.findById(id)
        if (optionalMeal.isEmpty())
            return Optional.empty()

        modifyAndSaveMeal(optionalMeal.get(), mealRequest)
    }

    @Override
    @Transactional
    void deleteMealWithId(Long id) {
        repository.deleteById(id)
    }

    Optional<Meal> modifyAndSaveMeal(Meal meal, MealRequest mealRequest) {
        meal.name = mealRequest.name
        meal.mealType = MealType.valueOf(mealRequest.type)
        meal.price = mealRequest.price
        meal = repository.saveAndFlush(meal)
        Optional.of(meal)
    }

    Meal createAndSaveMeal(MealRequest mealRequest) {
        repository.saveAndFlush(new Meal(
                name: mealRequest.name,
                mealType: MealType.valueOf(mealRequest.type),
                price: mealRequest.price))
    }
}
