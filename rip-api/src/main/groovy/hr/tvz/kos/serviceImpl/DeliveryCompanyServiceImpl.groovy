package hr.tvz.kos.serviceImpl

import groovy.util.logging.Slf4j
import hr.tvz.kos.config.ApplicationConfig
import hr.tvz.kos.mappers.DeliveryCompanyMapper
import hr.tvz.kos.model.DeliveryCompany
import hr.tvz.kos.payload.request.AddressRequest
import hr.tvz.kos.payload.request.DeliveryCompanyRequest
import hr.tvz.kos.payload.response.CompanyDeliveryListResponse
import hr.tvz.kos.payload.response.CompanyDeliveryResponse
import hr.tvz.kos.repository.DeliveryCompanyRepository
import hr.tvz.kos.service.DeliveryCompanyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Slf4j
@Service
class DeliveryCompanyServiceImpl implements DeliveryCompanyService {
    static final Double BASE_TIME_MEAL_PREPARATION = 20;

    @Autowired
    DeliveryCompanyRepository repository

    @Autowired
    ApplicationConfig config

    @Override
    List<DeliveryCompany> findAllCompanies() {
        return repository.findAll()
    }

    @Override
    List<CompanyDeliveryListResponse> calculateDeliveries(AddressRequest req) {
        def companies = repository.findAll()

        List<CompanyDeliveryResponse> companyList = new ArrayList<>()
        def distanceKm = distance(config.latitude, req.latitude, config.longitude, req.longitude)
        companies.each { it ->
            def cd = DeliveryCompanyMapper.toCompanyResponse(it)
            cd.totalPrice = Math.ceil(distanceKm * it.price)
            cd.totalTime = (Integer) Math.ceil(distanceKm * it.time + BASE_TIME_MEAL_PREPARATION)
            companyList.add(cd)

        }
        companyList as List<CompanyDeliveryListResponse>
    }

    @Override
    DeliveryCompany createDeliveryCompany(DeliveryCompanyRequest request) {
        repository.saveAndFlush(
                new DeliveryCompany(
                        name: request.name,
                        price: request.price,
                        time: request.time
                )
        )
    }

    @Override
    Optional<DeliveryCompany> modifyCompany(Long id, DeliveryCompanyRequest req) {
        Optional<DeliveryCompany> optionalCompany = repository.findById(id)
        if (optionalCompany.isEmpty())
            return Optional.empty()

        modifyAndSaveCompany(optionalCompany.get(), req)
    }

    @Override
    Optional<DeliveryCompany> fetchCompanyById(Long id) {
        repository.findById(id)
    }

    @Override
    void deleteCompanyWithId(Long id) {
        repository.deleteById(id)
    }

    static double distance(double lat1, double lat2, double lon1,
                           double lon2) {

        final int R = 6371 // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1)
        double lonDistance = Math.toRadians(lon2 - lon1)
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2)
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        double distance = R * c

        return distance
    }

    Optional<DeliveryCompany> modifyAndSaveCompany(DeliveryCompany company, DeliveryCompanyRequest request) {
        company.name = request.name
        company.time = request.time
        company.price = request.price
        company = repository.saveAndFlush(company)
        Optional.of(company)
    }
}
