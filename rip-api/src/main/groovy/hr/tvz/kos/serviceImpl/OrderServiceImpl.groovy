package hr.tvz.kos.serviceImpl

import groovy.util.logging.Slf4j
import hr.tvz.kos.exception.RipApiException
import hr.tvz.kos.model.Meal
import hr.tvz.kos.model.Order
import hr.tvz.kos.model.OrderItem
import hr.tvz.kos.model.User
import hr.tvz.kos.payload.request.OrderRequest
import hr.tvz.kos.repository.MealRepository
import hr.tvz.kos.repository.OrderItemRepository
import hr.tvz.kos.repository.OrderRepository
import hr.tvz.kos.repository.UserRepository
import hr.tvz.kos.service.OrderService
import org.springframework.stereotype.Service

import java.time.ZonedDateTime
import java.util.function.Supplier

import static java.util.Objects.requireNonNull

@Slf4j
@Service
class OrderServiceImpl implements OrderService {

    final OrderRepository orderRepository

    final OrderItemRepository orderItemRepository

    final UserRepository userRepository

    final MealRepository mealRepository

    OrderServiceImpl(UserRepository userRepository,
                     MealRepository mealRepository,
                     OrderItemRepository orderItemRepository,
                     OrderRepository orderRepository) {
        this.orderRepository = requireNonNull(orderRepository)
        this.orderItemRepository = requireNonNull(orderItemRepository)
        this.userRepository = requireNonNull(userRepository)
        this.mealRepository = requireNonNull(mealRepository)
    }

    @Override
    List<Order> getUserOrders(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username)

        userOptional.map(
                { user ->
                    orderRepository.findByUserOrderByCreateTimeDesc(user)
                }
        ).orElseThrow(
                new RipApiException("User with username ${username} doesn't exist") as Supplier<? extends Throwable>)

    }

    @Override
    Optional<Order> getOrderByUuid(String uuid) {
        orderRepository.findByUuid(uuid)
    }

    @Override
    Order createOrder(String username, OrderRequest orderRequest) {

        Optional<User> userOptional = userRepository.findByUsername(username)

        if (userOptional.isEmpty())
            throw new RipApiException("User does not exist for provided username")
        User user = userOptional.get()

        Order order = new Order()
        order.setCreateTime(ZonedDateTime.now())
        order.setDeliveryPrice(orderRequest.deliveryPrice)
        order.setDeliveryTime(orderRequest.deliveryTime)

        BigDecimal totalPrice = BigDecimal.ZERO

        List<OrderItem> orderItems = new ArrayList<>()

        for (Long itemId : orderRequest.itemList) {
            OrderItem orderItem = new OrderItem()

            Optional<Meal> mealOptional = mealRepository.findById(itemId)
            if (mealOptional.isEmpty())
                throw new RipApiException("There is no meal with id ${itemId}")
            Meal meal = mealOptional.get()
            orderItem.name = meal.name
            orderItem.type = meal.mealType.name()
            orderItem.price = meal.price
            totalPrice = totalPrice.add(meal.price)

            orderItems.add(orderItem)
        }
        Integer multiplier = totalPrice / 50
        Integer bonusPoints = user.points + multiplier * 5
        if (orderRequest.discount) {
            order.price = new BigDecimal(totalPrice) * new BigDecimal("0.8")
            bonusPoints -= 50
        } else {
            order.price = new BigDecimal(totalPrice.toString())
        }

        order.price = order.price.add(new BigDecimal(String.valueOf(order.deliveryPrice)))

        user.points = bonusPoints
        user = userRepository.save(user)

        order.orderItems = orderItems

        order.discount = orderRequest.discount
        order.uuid = UUID.randomUUID().toString()
        order.user = user
        order = orderRepository.saveAndFlush(order)

        for (OrderItem i : orderItems) {
            i.order = order
        }

        orderItemRepository.saveAll(orderItems)
        order
    }
}
