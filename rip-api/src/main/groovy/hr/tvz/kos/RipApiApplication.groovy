package hr.tvz.kos

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties

@SpringBootApplication
@EnableConfigurationProperties
class RipApiApplication {


    static void main(String[] args) {
        SpringApplication.run RipApiApplication, args
    }
}

