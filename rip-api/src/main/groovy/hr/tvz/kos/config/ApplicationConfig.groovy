package hr.tvz.kos.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component


@Component
class ApplicationConfig {

    @Value('${spring.mail.username}')
    String mailFrom

    @Value('${ripapi.jwt.jwt-secret}')
    String jwtSecret
    @Value('${ripapi.jwt.jwt-expiration-ms}')
    Integer jwtExpirationMs

    @Value('${ripapi.location.latitude}')
    Double latitude
    @Value('${ripapi.location.longitude}')
    Double longitude

}
