package hr.tvz.kos.security


import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails

class SecurityUtils {

    static Boolean checkUsername(String username) {
        def principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal()
        String realUsername = null
        if (principal instanceof UserDetails) {
            realUsername = ((UserDetails) principal).getUsername()
        } else {
            realUsername = principal.toString()
        }
        username.equals(realUsername)
    }
}
