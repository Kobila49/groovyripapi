package hr.tvz.kos.security.handlers

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler
import org.springframework.stereotype.Component

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j
@Component
class LogoutHandler implements LogoutSuccessHandler {

    @Override
    void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        try {
            SecurityContextHolder.getContext().setAuthentication(null)
            SecurityContextHolder.clearContext()
            String responseValue = new ObjectMapper().writeValueAsString("success")
            httpServletResponse.setStatus(HttpServletResponse.SC_ACCEPTED)
            httpServletResponse.addHeader("Content-Type", "application/json")
            httpServletResponse.getWriter().print(responseValue)
        }
        catch (Exception e) {
            log.error("Error", e)
            String responseValue
            try {
                responseValue = new ObjectMapper().writeValueAsString("failed")
                httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST)
                httpServletResponse.addHeader("Content-Type", "application/json")
                httpServletResponse.getWriter().print(responseValue)
            }
            catch (IOException e1) {
                log.error("Error", e1)
            }
        }
    }
}
