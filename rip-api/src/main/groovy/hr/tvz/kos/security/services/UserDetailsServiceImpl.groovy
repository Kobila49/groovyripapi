package hr.tvz.kos.security.services

import hr.tvz.kos.model.User
import hr.tvz.kos.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

import javax.transaction.Transactional

@Service
class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository repository

    @Override
    @Transactional
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username)
                .orElseThrow({ -> new UsernameNotFoundException("Invalid username or password") })
        return UserDetailsImpl.build(user)
    }
}
