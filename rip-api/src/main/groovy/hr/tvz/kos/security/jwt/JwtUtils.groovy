package hr.tvz.kos.security.jwt

import groovy.util.logging.Slf4j
import hr.tvz.kos.config.ApplicationConfig
import hr.tvz.kos.security.services.UserDetailsImpl
import io.jsonwebtoken.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

@Slf4j
@Component
class JwtUtils {

    @Autowired
    ApplicationConfig config

    String generateJwtToken(Authentication authentication) {
        def userPrincipal = (UserDetailsImpl) authentication.getPrincipal()

        Jwts.builder()
                .setSubject(userPrincipal.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + config.getJwtExpirationMs()))
                .signWith(SignatureAlgorithm.HS512, config.getJwtSecret())
                .compact()
    }

    String getUsernameFromJwtToken(String token) {
        Jwts.parser()
                .setSigningKey(config.getJwtSecret())
                .parseClaimsJws(token)
                .getBody()
                .getSubject()
    }

    boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser()
                    .setSigningKey(config.getJwtSecret())
                    .parseClaimsJws(authToken)
            return true
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage())
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage())
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage())
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage())
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage())
        }

        false
    }
}
