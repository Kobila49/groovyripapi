package hr.tvz.kos.service

import hr.tvz.kos.model.Meal
import hr.tvz.kos.payload.request.MealRequest

import javax.transaction.Transactional

interface MealService {

    @Transactional
    List<Meal> findAllMeals()

    Optional<Meal> getMealById(Long id)

    Meal createMeal(MealRequest mealRequest)

    Optional<Meal> modifyMeal(Long id, MealRequest mealRequest)

    void deleteMealWithId(Long id)
}