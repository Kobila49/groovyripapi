package hr.tvz.kos.service

import freemarker.template.TemplateException
import hr.tvz.kos.model.Mail

import javax.mail.MessagingException

interface MailService {

    void sendEmail(Mail mail) throws MessagingException, IOException, TemplateException

}
