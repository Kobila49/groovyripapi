package hr.tvz.kos.service

import hr.tvz.kos.model.Order
import hr.tvz.kos.payload.request.OrderRequest

interface OrderService {

    List<Order> getUserOrders(String username)

    Optional<Order> getOrderByUuid(String uuid)

    Order createOrder(String username, OrderRequest orderRequest)
}