package hr.tvz.kos.service

import hr.tvz.kos.model.DeliveryCompany
import hr.tvz.kos.payload.request.AddressRequest
import hr.tvz.kos.payload.request.DeliveryCompanyRequest
import hr.tvz.kos.payload.response.CompanyDeliveryListResponse

import javax.transaction.Transactional

interface DeliveryCompanyService {

    @Transactional
    List<DeliveryCompany> findAllCompanies()

    List<CompanyDeliveryListResponse> calculateDeliveries(AddressRequest addressRequest)

    DeliveryCompany createDeliveryCompany(DeliveryCompanyRequest deliveryCompanyRequest)

    Optional<DeliveryCompany> modifyCompany(Long id, DeliveryCompanyRequest deliveryCompanyRequest)

    Optional<DeliveryCompany> fetchCompanyById(Long id)

    void deleteCompanyWithId(Long id)
}