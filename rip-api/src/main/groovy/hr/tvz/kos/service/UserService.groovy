package hr.tvz.kos.service

import hr.tvz.kos.model.Role
import hr.tvz.kos.model.User
import hr.tvz.kos.payload.request.SignupRequest
import hr.tvz.kos.payload.request.UserRequest

import javax.transaction.Transactional

interface UserService {

    @Transactional
    List<User> fetchAllUsers()

    @Transactional
    List<Role> fetchAllRoles()

    @Transactional
    Optional<User> createUser(UserRequest userRequest)

    @Transactional
    Optional<User> editUser(UserRequest userRequest, Long id)

    @Transactional
    Optional<User> registerNormalUser(SignupRequest signupRequest)

    @Transactional
    Optional<User> modifyNormalUser(SignupRequest signupRequest, String username)

    @Transactional
    Optional<User> findUserByUsername(def username)

    @Transactional
    Optional<User> findById(Long id)

    @Transactional
    void deleteUser(Long id)

}
