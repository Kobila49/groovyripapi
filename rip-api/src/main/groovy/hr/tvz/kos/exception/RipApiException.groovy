package hr.tvz.kos.exception

class RipApiException extends RuntimeException {

    static final long serialVersionUID = 3789252423033362059L

    RipApiException() {}

    RipApiException(String message) {
        super(message)
    }

}
