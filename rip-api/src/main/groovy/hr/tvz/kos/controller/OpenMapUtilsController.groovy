package hr.tvz.kos.controller

import hr.tvz.kos.payload.request.AddressRequest
import hr.tvz.kos.payload.response.CoordinateResponse
import hr.tvz.kos.utils.OpenStreetMapUtils
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import static org.springframework.http.ResponseEntity.ok

@RestController
@RequestMapping("/api/util/map")
class OpenMapUtilsController {


    @PostMapping("/coordinates")
    ResponseEntity<CoordinateResponse> getCoordinates(@RequestBody AddressRequest address) {

        def coordinates = OpenStreetMapUtils.getInstance().getCoordinates(address.getAddress())

        def response = new CoordinateResponse()
        response.setLongitude(coordinates.get('lon'))
        response.setLatitude(coordinates.get('lat'))
        ok(response)
    }

}
