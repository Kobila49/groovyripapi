package hr.tvz.kos.controller

import groovy.util.logging.Slf4j
import hr.tvz.kos.config.ApplicationConfig
import hr.tvz.kos.model.Mail
import hr.tvz.kos.payload.request.OrderRequest
import hr.tvz.kos.payload.response.OrderListResponse
import hr.tvz.kos.payload.response.OrderResponse
import hr.tvz.kos.security.SecurityUtils
import hr.tvz.kos.service.MailService
import hr.tvz.kos.service.OrderService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

import static hr.tvz.kos.mappers.OrderMapper.toOrderResponse
import static hr.tvz.kos.mappers.OrderMapper.toOrderResponseList
import static java.util.Objects.requireNonNull

@Slf4j
@RestController
@RequestMapping(path = "/api/web")
class OrderController {

    final ApplicationConfig config

    final OrderService orderService

    final MailService mailService

    OrderController(ApplicationConfig config,
                    OrderService orderService,
                    MailService mailService) {
        this.config = requireNonNull(config)
        this.orderService = requireNonNull(orderService)
        this.mailService = requireNonNull(mailService)
    }


    @Secured(['ROLE_USER'])
    @GetMapping(value = "/{username}/orders")
    def getOrders(@PathVariable(value = "username") String username) {
        if (SecurityUtils.checkUsername(username)) {
            log.info("Fetching orders for user with username ${username}")
            OrderListResponse body = new OrderListResponse(orders: toOrderResponseList(orderService.getUserOrders(username)))
            if (body.orders.size() > 0) {
                ResponseEntity.status(HttpStatus.OK).body(body)
            } else {
                ResponseEntity.status(HttpStatus.NO_CONTENT).build()
            }
        } else {

            ResponseEntity.status(HttpStatus.FORBIDDEN).build()
        }
    }

    @Secured(['ROLE_USER'])
    @GetMapping(value = "/orders/{orderUuid}")
    def getOrder(@PathVariable(value = "orderUuid") String orderUuid) {
        orderService.getOrderByUuid(orderUuid).map(
                { order ->
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(toOrderResponse(order))
                }
        ).orElseGet(
                { ->
                    ResponseEntity.
                            status(HttpStatus.NOT_FOUND)
                            .build()
                }
        )
    }


    @Secured(['ROLE_USER'])
    @PostMapping("/{username}/orders")
    def saveUserOrder(@PathVariable(value = "username") String username, @Valid @RequestBody OrderRequest order) {
        if (SecurityUtils.checkUsername(username)) {
            OrderResponse body = toOrderResponse(orderService.createOrder(username, order))
            mailService.sendEmail(createMail(body))
            ResponseEntity.status(HttpStatus.OK).body(body)
        } else {
            ResponseEntity.status(HttpStatus.FORBIDDEN).build()
        }
    }


    Mail createMail(OrderResponse response) {
        Mail mail = new Mail()
        mail.mailFrom = config.mailFrom
        mail.mailTo = response.user.email
        mail.mailSubject = "Potvrda narudžbe " + response.uuid

        Map model = new HashMap()
        model.put("name", response.user.firstName)
        model.put("orderList", response.orderItems)
        model.put("price", response.price)
        mail.model = model
        mail
    }
}