package hr.tvz.kos.controller

import hr.tvz.kos.model.User
import hr.tvz.kos.payload.request.LoginRequest
import hr.tvz.kos.payload.request.SignupRequest
import hr.tvz.kos.payload.response.JwtResponse
import hr.tvz.kos.payload.response.MessageResponse
import hr.tvz.kos.security.jwt.JwtUtils
import hr.tvz.kos.security.services.UserDetailsImpl
import hr.tvz.kos.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import javax.validation.Valid
import java.util.stream.Collectors

@RestController
@RequestMapping("/api/auth")
class AuthController {

    @Autowired
    AuthenticationManager authenticationManager

    @Autowired
    UserService userService

    @Autowired
    JwtUtils jwtUtils

    @PostMapping("/login")
    ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.username, loginRequest.password)
        )

        SecurityContextHolder.getContext().setAuthentication(authentication)
        String jwt = jwtUtils.generateJwtToken(authentication)

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal()
        List<String> roles = userDetails.getAuthorities().stream()
                .map({ item -> item.getAuthority() })
                .collect(Collectors.toList())

        ResponseEntity.ok(
                new JwtResponse(jwt,
                        userDetails.getId(),
                        userDetails.getUsername(),
                        userDetails.getEmail(),
                        roles)
        )
    }

    @PostMapping("/register")
    ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        Optional<User> user = userService.registerNormalUser(signupRequest)
        if (user.isPresent()) {
            ResponseEntity.ok().build()
        } else {
            ResponseEntity.badRequest().body(new MessageResponse("Username or email is already in use!"))
        }

    }
}
