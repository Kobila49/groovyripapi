package hr.tvz.kos.controller

import groovy.util.logging.Slf4j
import hr.tvz.kos.payload.request.SignupRequest
import hr.tvz.kos.payload.request.UserRequest
import hr.tvz.kos.payload.response.MessageResponse
import hr.tvz.kos.security.SecurityUtils
import hr.tvz.kos.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

import static hr.tvz.kos.mappers.RoleMapper.toRoleListResponse
import static hr.tvz.kos.mappers.UserMapper.toUserListResponse
import static hr.tvz.kos.mappers.UserMapper.toUserResponse

@Slf4j
@RestController
@RequestMapping(path = "api/users")
@CrossOrigin(origins = "http://localhost:4200")
class UserController {

    @Autowired
    UserService userDetailsService

    @GetMapping("/{username}")
    def getUsersDetails(@PathVariable(value = "username") def username) {
        userDetailsService.findUserByUsername(username).map(
                { user ->
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(toUserResponse(user))
                }
        )
                .orElseGet(
                        { ->
                            ResponseEntity
                                    .status(HttpStatus.NOT_FOUND)
                                    .build()
                        })

    }

    @Secured(['ROLE_ADMIN'])
    @GetMapping("/details/{id}")
    def fetchUserById(@PathVariable(value = "id") Long id) {
        userDetailsService.findById(id).map(
                { user ->
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(toUserResponse(user))
                }
        )
                .orElseGet(
                        { ->
                            ResponseEntity
                                    .status(HttpStatus.NOT_FOUND)
                                    .build()
                        })
    }

    @Secured(['ROLE_USER'])
    @PutMapping("/modify/{username}")
    def modifyUserDetails(@PathVariable(value = "username") def username, @Valid @RequestBody SignupRequest signupRequest) {
        if (SecurityUtils.checkUsername(username)) {
            userDetailsService.modifyNormalUser(signupRequest, username).map(
                    { user ->
                        ResponseEntity
                                .status(HttpStatus.ACCEPTED)
                                .body(toUserResponse(user))
                    }
            ).orElseGet({ ->
                ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            }

            )
        } else {
            ResponseEntity.status(HttpStatus.FORBIDDEN).build()
        }

    }

    @Secured(['ROLE_ADMIN'])
    @PostMapping("/create")
    def createUserWithCustomRoles(@Valid @RequestBody UserRequest userRequest) {
        userDetailsService.createUser(userRequest).map(
                { user ->
                    ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(toUserResponse(user))
                }
        ).orElseGet(
                { ->
                    ResponseEntity
                            .badRequest()
                            .body(new MessageResponse("Username or email is already in use!"))
                }
        )
    }

    @Secured(['ROLE_ADMIN'])
    @PutMapping("/edit/{id}")
    def editUser(@PathVariable(value = "id") Long id, @Valid @RequestBody UserRequest userRequest) {
        userDetailsService.editUser(userRequest, id).map(
                { user ->
                    ResponseEntity
                            .status(HttpStatus.ACCEPTED)
                            .body(toUserResponse(user))
                }
        ).orElseGet(
                { ->
                    ResponseEntity
                            .badRequest()
                            .body(new MessageResponse("Username or email is already in use!"))
                }
        )
    }

    @Secured(['ROLE_ADMIN'])
    @GetMapping()
    def getAllUsers() {
        ResponseEntity
                .status(HttpStatus.OK)
                .body(toUserListResponse(userDetailsService.fetchAllUsers()))
    }

    @Secured(['ROLE_ADMIN'])
    @GetMapping("/roles")
    def getAllRoles() {
        ResponseEntity
                .status(HttpStatus.OK)
                .body(toRoleListResponse(userDetailsService.fetchAllRoles()))
    }

    @Secured(['ROLE_ADMIN'])
    @DeleteMapping("/{id}")
    def deleteUser(@PathVariable(value = "id") Long id) {
        ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(userDetailsService.deleteUser(id))
    }

}

