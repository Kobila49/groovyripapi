package hr.tvz.kos.controller

import groovy.util.logging.Slf4j
import hr.tvz.kos.payload.request.AddressRequest
import hr.tvz.kos.payload.request.DeliveryCompanyRequest
import hr.tvz.kos.service.DeliveryCompanyService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

import static hr.tvz.kos.mappers.DeliveryCompanyMapper.toCompanyListResponse
import static hr.tvz.kos.mappers.DeliveryCompanyMapper.toCompanyResponse
import static java.util.Objects.requireNonNull

@Slf4j
@RestController
@RequestMapping(path = "/api/web/delivery/companies")
class DeliveryCompanyController {

    final DeliveryCompanyService service

    DeliveryCompanyController(DeliveryCompanyService service) {
        this.service = requireNonNull(service)
    }

    @Secured(['ROLE_ADMIN'])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    def getCompanies() {
        toCompanyListResponse(service.findAllCompanies())
    }

    @Secured(['ROLE_ADMIN'])
    @GetMapping("/{id}")
    def getCompanyById(@PathVariable(name = "id") Long id) {
        service.fetchCompanyById(id).map(
                { company ->
                    ResponseEntity
                            .status(HttpStatus.ACCEPTED)
                            .body(toCompanyResponse(company))
                }
        ).orElseGet({ ->
            ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        })
    }


    @Secured(['ROLE_ADMIN'])
    @PostMapping()
    def createDeliveryCompany(@Valid @RequestBody DeliveryCompanyRequest companyRequest) {
        ResponseEntity
                .status(HttpStatus.CREATED)
                .body(toCompanyResponse(service.createDeliveryCompany(companyRequest)))
    }

    @Secured(['ROLE_ADMIN'])
    @PutMapping("/{id}")
    def editDeliveryCompany(@PathVariable(name = "id") Long id, @Valid @RequestBody DeliveryCompanyRequest companyRequest) {
        service.modifyCompany(id, companyRequest).map(
                { company ->
                    ResponseEntity
                            .status(HttpStatus.ACCEPTED)
                            .body(toCompanyResponse(company))
                }
        ).orElseGet({ ->
            ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        })
    }

    @Secured(['ROLE_ADMIN'])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    def deleteMeal(@PathVariable(name = "id") Long id) {
        service.deleteCompanyWithId(id)
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/calculate")
    def getPriceForDelivery(@RequestBody AddressRequest address) {
        service.calculateDeliveries(address)
    }
}
