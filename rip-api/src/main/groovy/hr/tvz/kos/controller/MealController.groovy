package hr.tvz.kos.controller

import groovy.util.logging.Slf4j
import hr.tvz.kos.payload.request.MealRequest
import hr.tvz.kos.service.MealService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

import static hr.tvz.kos.mappers.MealMapper.toMealListResponse
import static hr.tvz.kos.mappers.MealMapper.toMealResponse
import static java.util.Objects.requireNonNull

@Slf4j
@RestController
@RequestMapping(path = "/api/web/meals")
class MealController {

    final MealService mealService

    MealController(MealService mealService) {
        this.mealService = requireNonNull(mealService)
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("")
    def getMeals() {
        toMealListResponse(mealService.findAllMeals())
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODERATOR'])
    @GetMapping(value = "/{id}")
    def getMeal(@PathVariable(value = "id") Long id) {
        mealService.getMealById(id).map(
                { meal ->
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(toMealResponse(meal))
                }
        ).orElseGet(
                { ->
                    ResponseEntity.
                            status(HttpStatus.NOT_FOUND)
                            .build()
                }
        )
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODERATOR'])
    @PostMapping()
    def createMeal(@Valid @RequestBody MealRequest mealRequest) {
        ResponseEntity
                .status(HttpStatus.CREATED)
                .body(toMealResponse(mealService.createMeal(mealRequest)))
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODERATOR'])
    @PutMapping("/{id}")
    def editMeal(@PathVariable(name = "id") Long id, @Valid @RequestBody MealRequest mealRequest) {
        mealService.modifyMeal(id, mealRequest).map(
                { meal ->
                    ResponseEntity
                            .status(HttpStatus.ACCEPTED)
                            .body(toMealResponse(meal))
                }
        ).orElseGet({ ->
            ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        })
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODERATOR'])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    def deleteMeal(@PathVariable(name = "id") Long id) {
        mealService.deleteMealWithId(id)
    }
}
