package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude
import groovy.transform.ToString

@ToString(includeNames = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class OrderResponse {

    String uuid

    String createdAt

    UserResponse user

    List<OrderItemResponse> orderItems

    BigDecimal price

    BigDecimal deliveryPrice

    BigDecimal deliveryTime

    Boolean discount

}
