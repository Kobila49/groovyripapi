package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class OrderItemResponse {

    String name

    String type

    BigDecimal price
}
