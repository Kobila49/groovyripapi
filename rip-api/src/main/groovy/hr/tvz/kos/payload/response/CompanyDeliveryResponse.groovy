package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CompanyDeliveryResponse {

    Long id

    String name

    Double price

    Integer time

    Integer totalTime

    Double totalPrice
}