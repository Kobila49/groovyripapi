package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class OrderListResponse {

    List<OrderResponse> orders
}
