package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude
import hr.tvz.kos.model.Role

@JsonInclude(JsonInclude.Include.NON_NULL)
class RoleListResponse {

    List<Role> roles
}
