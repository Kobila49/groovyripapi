package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude
import hr.tvz.kos.enums.MealType

@JsonInclude(JsonInclude.Include.NON_NULL)
class MealResponse {

    Long id

    String name

    MealType type

    BigDecimal price
}
