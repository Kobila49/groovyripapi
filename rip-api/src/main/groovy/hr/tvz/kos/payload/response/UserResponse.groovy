package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class UserResponse {

    Long id

    String username

    String firstName

    String lastName

    String address

    Double latitude

    Double longitude

    String email

    String phoneNumber

    String role

    BigInteger points

}
