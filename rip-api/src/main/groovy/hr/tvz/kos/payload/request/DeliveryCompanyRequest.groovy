package hr.tvz.kos.payload.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class DeliveryCompanyRequest {

    @NotBlank
    String name

    @NotNull
    Double price

    @NotNull
    Double time

}
