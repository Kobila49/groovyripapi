package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class RestaurantResponse {

    Long id

    String name

    String address

}
