package hr.tvz.kos.payload.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class JwtResponse {
    String token
    String type = "Bearer"
    Long id
    String username
    String email
    List<String> roles

    // default constructor for jackson
    JwtResponse() {}

    JwtResponse(String accessToken, Long id, String username, String email, List<String> roles) {
        this.token = accessToken
        this.id = id
        this.username = username
        this.email = email
        this.roles = roles
    }

}