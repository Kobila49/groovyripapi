package hr.tvz.kos.payload.response


import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CoordinateResponse {

    Double longitude
    Double latitude

    CoordinateResponse() {}
}
