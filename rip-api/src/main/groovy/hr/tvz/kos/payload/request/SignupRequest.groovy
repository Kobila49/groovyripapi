package hr.tvz.kos.payload.request

import com.fasterxml.jackson.annotation.JsonInclude
import groovy.transform.ToString

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
class SignupRequest {

    @NotBlank(message = "Username is mandatory")
    String username

    @NotBlank(message = "First name is mandatory")
    String firstName

    @NotBlank(message = "Last name is mandatory")
    String lastName

    @NotBlank(message = "Address is mandatory")
    String address

    Double longitude

    Double latitude

    @NotBlank(message = "Password is mandatory")
    @Size(min = 6, message = "Password must have at least 6 chars")
    String password

    @NotBlank(message = "Email is mandatory")
    @Email(message = "Email is not in good format")
    String email

    @NotBlank(message = "Phone number is mandatory")
    @Size(min = 8, message = "Phone number must have at least 8 chars")
    String phoneNumber
}
