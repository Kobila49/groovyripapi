package hr.tvz.kos.payload.request

import com.fasterxml.jackson.annotation.JsonInclude
import groovy.transform.ToString

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.PositiveOrZero

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
class MealRequest {

    @NotBlank(message = "Name must not be empty")
    String name

    @NotBlank(message = "Type must not be empty")
    String type

    @NotNull(message = "Price must be entered")
    @PositiveOrZero(message = "Price must be entered as a positive integer")
    BigDecimal price
}
