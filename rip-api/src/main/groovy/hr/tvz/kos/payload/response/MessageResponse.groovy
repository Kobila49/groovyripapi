package hr.tvz.kos.payload.response

class MessageResponse {
    String message

    MessageResponse(String message) {
        this.message = message
    }
}
