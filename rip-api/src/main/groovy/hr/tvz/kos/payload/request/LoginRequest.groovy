package hr.tvz.kos.payload.request

import javax.validation.constraints.NotBlank

class LoginRequest {

    @NotBlank
    String username

    @NotBlank
    String password
}
