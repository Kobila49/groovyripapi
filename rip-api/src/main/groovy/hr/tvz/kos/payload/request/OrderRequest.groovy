package hr.tvz.kos.payload.request

import javax.validation.constraints.NotNull

class OrderRequest {

    @NotNull
    List<Long> itemList

    @NotNull
    Boolean discount

    @NotNull
    Double deliveryTime

    @NotNull
    Double deliveryPrice
}
