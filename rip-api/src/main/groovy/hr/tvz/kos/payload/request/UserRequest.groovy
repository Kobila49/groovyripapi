package hr.tvz.kos.payload.request

import com.fasterxml.jackson.annotation.JsonInclude
import groovy.transform.ToString

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
class UserRequest {

    @NotBlank(message = "Username is mandatory")
    String username

    String firstName

    String lastName

    String address

    @NotBlank(message = "Password is mandatory")
    @Size(min = 6, message = "Password must have at least 6 chars")
    String password

    @NotBlank(message = "Email is mandatory")
    @Email(message = "Email is not in good format")
    String email

    String phoneNumber

    @NotNull
    Long role

    Integer points

}
