package hr.tvz.kos.payload.request

class AddressRequest {

    String address

    Double latitude

    Double longitude
}
