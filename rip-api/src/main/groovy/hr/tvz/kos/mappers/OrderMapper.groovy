package hr.tvz.kos.mappers

import hr.tvz.kos.model.Order
import hr.tvz.kos.payload.response.OrderResponse

import java.time.format.DateTimeFormatter

import static hr.tvz.kos.mappers.OrderItemMapper.toOrderItems
import static hr.tvz.kos.mappers.UserMapper.toUserResponse

class OrderMapper {

    static OrderResponse toOrderResponse(Order order) {
        OrderResponse orderResponse = new OrderResponse()
        orderResponse.uuid = order.uuid
        orderResponse.createdAt = order.createTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        orderResponse.user = toUserResponse(order.user)
        orderResponse.orderItems = toOrderItems(order.orderItems)
        orderResponse.price = order.price
        orderResponse.deliveryPrice = order.deliveryPrice
        orderResponse.deliveryTime = order.deliveryTime
        orderResponse.discount = order.discount
        orderResponse
    }

    static List<OrderResponse> toOrderResponseList(List<Order> orders) {
        List<OrderResponse> orderResponseList = new ArrayList<>()
        orders.each {
            orderResponseList.add(toOrderResponse(it))
        }
        orderResponseList
    }
}
