package hr.tvz.kos.mappers

import hr.tvz.kos.model.User
import hr.tvz.kos.payload.response.UserListResponse
import hr.tvz.kos.payload.response.UserResponse

import static hr.tvz.kos.mappers.RoleMapper.extractRole

class UserMapper {

    static UserResponse toUserResponse(User user) {
        new UserResponse(
                id: user.id,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                address: user.address,
                latitude: user.latitude,
                longitude: user.longitude,
                email: user.email,
                phoneNumber: user.phoneNumber,
                points: user.points,
                role: extractRole(user.roles)
        )
    }

    static UserListResponse toUserListResponse(List<User> users) {
        List<UserResponse> userResponseList = new ArrayList<>()
        users.each {
            userResponseList.add(toUserResponse(it))
        }
        new UserListResponse(users: userResponseList)
    }
}
