package hr.tvz.kos.mappers

import hr.tvz.kos.model.DeliveryCompany
import hr.tvz.kos.payload.response.CompanyDeliveryListResponse
import hr.tvz.kos.payload.response.CompanyDeliveryResponse

class DeliveryCompanyMapper {

    static CompanyDeliveryListResponse toCompanyListResponse(List<DeliveryCompany> companies) {
        List<CompanyDeliveryResponse> companyListResponse = new ArrayList<>()
        companies.each {
            CompanyDeliveryResponse companyResponse = toCompanyResponse(it)
            companyListResponse.add(companyResponse)
        }
        new CompanyDeliveryListResponse(companies: companyListResponse)
    }

    static CompanyDeliveryResponse toCompanyResponse(DeliveryCompany it) {
        new CompanyDeliveryResponse(id: it.id,
                name: it.name,
                price: it.price,
                time: it.time)
    }
}
