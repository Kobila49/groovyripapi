package hr.tvz.kos.mappers

import hr.tvz.kos.model.OrderItem
import hr.tvz.kos.payload.response.OrderItemResponse

class OrderItemMapper {

    static List<OrderItemResponse> toOrderItems(List<OrderItem> orderItems) {
        List<OrderItemResponse> orderItemResponseList = new ArrayList<>()
        orderItems.each {
            orderItemResponseList.add(
                    new OrderItemResponse(name: it.name,
                            price: it.price,
                            type: it.type))
        }
        orderItemResponseList
    }
}
