package hr.tvz.kos.mappers

import hr.tvz.kos.model.Role
import hr.tvz.kos.payload.response.RoleListResponse

class RoleMapper {

    static String extractRole(Set<Role> roles) {
        if (roles.size() == 1) {
            roles.toList().get(0).name.name()
        }
    }

    static RoleListResponse toRoleListResponse(List<Role> roles) {
        new RoleListResponse(roles: roles)
    }
}
