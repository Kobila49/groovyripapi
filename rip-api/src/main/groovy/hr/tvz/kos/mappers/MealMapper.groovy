package hr.tvz.kos.mappers


import hr.tvz.kos.model.Meal
import hr.tvz.kos.payload.response.MealListResponse
import hr.tvz.kos.payload.response.MealResponse

class MealMapper {

    static MealListResponse toMealListResponse(List<Meal> meals) {
        List<MealResponse> mealResponseList = new ArrayList<>()
        meals.each {
            MealResponse mealResponse = toMealResponse(it)
            mealResponseList.add(mealResponse)
        }
        new MealListResponse(meals: mealResponseList)
    }

    static MealResponse toMealResponse(Meal it) {
        new MealResponse(id: it.id, name: it.name, type: it.mealType, price: it.price)
    }

}
