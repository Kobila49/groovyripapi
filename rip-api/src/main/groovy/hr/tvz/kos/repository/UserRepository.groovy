package hr.tvz.kos.repository


import hr.tvz.kos.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(def username)

    Boolean existsByUsername(String username)

    Boolean existsByEmailIgnoreCase(String email)

    Boolean existsByUsernameAndIdNot(String username, Long id)

    Boolean existsByEmailIgnoreCaseAndIdNot(String email, Long id)

}
