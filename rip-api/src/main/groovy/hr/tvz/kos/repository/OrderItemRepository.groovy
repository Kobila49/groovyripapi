package hr.tvz.kos.repository

import hr.tvz.kos.model.OrderItem
import org.springframework.data.jpa.repository.JpaRepository

interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
}
