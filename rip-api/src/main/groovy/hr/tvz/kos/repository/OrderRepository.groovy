package hr.tvz.kos.repository

import hr.tvz.kos.model.Order
import hr.tvz.kos.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByUserOrderByCreateTimeDesc(User userAccount)

    Optional<Order> findByUuid(String uuid)
}
