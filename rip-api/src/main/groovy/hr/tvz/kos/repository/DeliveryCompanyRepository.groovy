package hr.tvz.kos.repository

import hr.tvz.kos.model.DeliveryCompany
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DeliveryCompanyRepository extends JpaRepository<DeliveryCompany, Long> {

}