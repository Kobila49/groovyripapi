package hr.tvz.kos.repository

import hr.tvz.kos.model.Meal
import org.springframework.data.jpa.repository.JpaRepository

interface MealRepository extends JpaRepository<Meal, Long> {

}
