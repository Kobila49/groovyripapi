package hr.tvz.kos.repository

import hr.tvz.kos.enums.ERole
import hr.tvz.kos.model.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(ERole name)

}