<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sending Email with Freemarker HTML Template Example</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- use the font -->
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            font-size: 48px;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
    <tr>
        <td align="center" bgcolor="#FF0000" style="padding: 40px 0 30px 0;">
            <img src="cid:rip-logo.png" alt="https://rip.com" style="display: block;"/>
        </td>
    </tr>
    <tr>
        <td bgcolor="#F0FFFF" style="padding: 40px 30px 40px 30px;">
            <p>Dear ${name},</p>
            <p>Your order is confirmed!</b></p>
            <p>Regards, RIP Support</p>
        </td>
    </tr>
    <tr>
        <td bgcolor="#F0FFFF" style="padding: 30px 30px 30px 30px;">
            <table>
                <tr>
                    <td>Name</td>
                    <td>Type</td>
                    <td>Price</td>
                </tr>
                <#list orderList as orderItem>
                    <tr class="${orderItem?item_parity}Row">
                        <td>${orderItem.name}</td>
                        <td>${orderItem.type}</td>
                        <td>${orderItem.price}</td>
                    </tr>
                </#list>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFF00" style="padding: 30px 30px 30px 30px;">Total Amount Charge: ${price}</td>
        <td bgcolor="#FFFF00" style="padding: 30px 30px 30px 30px;">Total Amount Charge: ${price}</td>
    </tr>
</table>

</body>
</html>
