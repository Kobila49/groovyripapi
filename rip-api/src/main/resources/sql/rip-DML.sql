insert into meal (meal_type, name, price)
values ('BURGER', 'Cheeseburger', 30.0);
insert into meal (meal_type, name, price)
values ('PIZZA', 'Capricciosa', 45.0);

INSERT INTO role (name)
VALUES ('ROLE_ADMIN');
INSERT INTO role (name)
VALUES ('ROLE_USER');
INSERT INTO role (name)
VALUES ('ROLE_MODERATOR');

insert into users (address, email, first_name, last_name, password, phone_number, points, username)
values ('NOT_SET',
        'admin@rip.com',
        'NOT_SET',
        'NOT_SET',
        '$2a$10$kBFN6tiVuM8.bcyNW2QtbOSt8swTcVXdyTzIfGE1ngp/YGdNJn/7C',
        'NOT_SET',
        0,
        'admin');
insert into user_role
values (1, 1);

insert into delivery_company (id, name, price, time)
values (1, 'Wolt', 4, 6);
insert into delivery_company (id, name, price, time)
values (2, 'Glovo', 6, 4);